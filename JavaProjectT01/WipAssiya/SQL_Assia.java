/**
 * 
 */
package WibAssiya;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Miguel Angel
 *
 */
public class SQL {
	
	private static Connection Conexion = null;
	


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
 //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		
		 

		//TABLA USUARIOS
		  public void insertDataUsuarios(String USUARIO, String CLAVE, String NOMBRE, String APELLIDO, String EMAIL, int CODIGO_POSTAL, String ULTIMO_VIAJE, String PAIS_DESTINO, String OTROS_DATOS) {
			    try {
			        String Query = "INSERT INTO " + "USUARIOS" + " VALUES("
			            	+ "'"+ USUARIO + "',"
			                + "'"+ CLAVE + "',"
			            	+ "'"+ NOMBRE + "',"
			            	+ "'"+ APELLIDO + "',"
			            	+ "'"+ EMAIL + "',"
			            	+ "'"+ CODIGO_POSTAL + "',"
			            	+ "'"+ ULTIMO_VIAJE + "',"
			            	+ "'"+ PAIS_DESTINO + "',"
			            	+ "'"+ OTROS_DATOS + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }	
			
		//piases
		  
		  public void insertDataPaises(String NOMBRE, String CAPITAL,String MONEDA, String IDIOMA, String BANDERA, int HABITANTES, String CONTINENTE) {
			    try {
			        String Query = "INSERT INTO " + "PAISES" + " VALUES("
			            	+ "'"+ NOMBRE + "',"
			                + "'"+ MONEDA + "',"
			            	+ "'"+ IDIOMA + "',"
			            	+ "'"+ BANDERA + "',"
			            	+ "'"+ CAPITAL + "',"
			            	+ "'"+ HABITANTES + "',"	            	
			            	+ "'"+ CONTINENTE + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
		//	 ciudad
		  public void insertDataCiudadesPueblos(int ID_CIUDAD, String NOMBRE, String PAIS,String DESCRIPCION) {
			  try {
			        String Query = "INSERT INTO " + "CIUDADES_PUEBLOS" + " VALUES("
			            	+ "'"+ ID_CIUDAD + "',"
			                + "'"+ NOMBRE + "',"
			            	+ "'"+ PAIS + "',"	            	
			            	+ "'"+ DESCRIPCION + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		   } catch (SQLException ex) {
		       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		   }
	   } 
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		  public void getValues(String tabla,String pais) {
		        try {
		            String Query = "SELECT * FROM " + tabla+ " WHERE NOMBRE = '" + pais + "'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("Id_restaurante: " + resultSet.getString("NOMBRE") + " "
		                        + "Usuario: " + resultSet.getString("MONEDA") + " " 
								+ "Clave: "+ resultSet.getString("CAPITAL") + " "
		                        + "Opinar: " + resultSet.getString("BANDERA") + " "
		                        + "data_comentario: " + resultSet.getString("CONTINENTE"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
		  public String[] getValuesPais (String tabla)
		  {
		        String num[]=new String[20];
		        int cont=0; 
		        int tamany;
		        try {
		            String Query = "SELECT * FROM " + tabla;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		            
			      System.out.println("hhhhhh:   "+resultSet.getRow());
		            //tamany=resultSet.getRow();
		            while (resultSet.next()) {
		            	
		    	        num[cont]=resultSet.getString("NOMBRE");
		            	cont++;
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return num;
		    }
	// obtener datos de ciudad
			 public void getValuesCiudad(String ciudad, String pais) {
				 

			        try {
				           String Query = "SELECT * FROM " + "CIUDADES_PUEBLOS" + " WHERE NOMBRE = '" + ciudad + "'"+" and NOMBRE_PAIS = '" + pais+ "'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            System.out.println(ciudad);
			            while (resultSet.next()) {
				                System.out.println("Nombre: " + resultSet.getString("ID_CIUDAD") + " "
				                + "Descripcion: " + resultSet.getString("NOMBRE") + " " 
		                        + "Descripcion: " + resultSet.getString("NOMBRE_PAIS") + " " 
		                        + "pais: " + resultSet.getString("DESCRIPCION"));
			            }

			        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
				    }
			 

		 
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(String table_name, String ID) {
		        try {
		            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		 }
		      

}
