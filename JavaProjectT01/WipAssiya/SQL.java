/**
 * 
 */
package WipAssiya;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * @author Assiya
 *
 */
public class SQL {
	
	private static Connection Conexion = null;
	


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
 //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		

		//TABLA USUARIOS
		  public void insertDataUsuarios(String USUARIO,String CLAVE, String NOMBRE, String APELLIDO, String EMAIL, int CODIGO_POSTAL, String ULTIMO_VIAJE, String PAIS_DESTINO, String OTROS_DATOS) {
			  System.out.println(USUARIO+" "+CLAVE+" "+NOMBRE+" "+APELLIDO+" "+EMAIL+" "+CODIGO_POSTAL+" "+ULTIMO_VIAJE+" "+PAIS_DESTINO+" "+OTROS_DATOS); 
			  try {
			        String Query = "INSERT INTO " + "USUARIOS" + " VALUES("
			            	+ "'"+ USUARIO + "',"
			                + "'"+ CLAVE + "',"
			            	+ "'"+ NOMBRE + "',"
			            	+ "'"+ APELLIDO + "',"
			            	+ "'"+ EMAIL + "',"
			            	+ "'"+ CODIGO_POSTAL + "',"
			            	+ "'"+ ULTIMO_VIAJE + "',"
			            	+ "'"+ PAIS_DESTINO + "',"
			                + "'"+ OTROS_DATOS +  "')";
			        
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
			  
		   }
		//...................................................EDITAR VALORES DE LA TABLA USUARIO...................................................
		 
		  public void ModificarUsuario(String nombres, String Apellidos, String usuari) {
			  

			  try {
		          /*  String Query = "Update USUARIOS set NOMBRE='" +nombre+"'"+" APELLIDO='" +Apellido+ "'"+
		            				"where USUARIO='"+usuario+"'";
				  
				 // String Query = "UPDATE USUARIOS SET NOMBRE=nombre, APELLIDO=Apellido"
		                     //     + "WHERE USUARIO=usuario";
		         // Statement comando=Conexion.createStatement();
				  
				 // String Query = "update USUARIOS SET NOMBRE='Miquel', Apellido='dos' where USUARIO='hola'";
				  
				  PreparedStatement prest = Conexion.prepareStatement(Query);
				  prest.executeUpdate(Query);           

		             // prest.executeUpdate(Query);*/
				  
				  // create our java preparedstatement using a sql update query
				  String usuario;
				  String clave;
				  String nombre;
				  String apellido;
				  
				    PreparedStatement ps = Conexion.prepareStatement(
				      "UPDATE Messages SET nombre = ?, apellido = ? WHERE usuario = ?");

				    // set the preparedstatement parameters
				  //  ps.setString(1,description);
				  //  ps.setString(2,author);
				  //  ps.setInt(3,id);
				  //  ps.setInt(4,seqNum);

				    // call executeUpdate to execute our sql update statement
				    ps.executeUpdate();
				    ps.close();

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error en modificar el registro especificado");
		        }
		 }	

		  
//...................................................METODO OBTENER VALORES DE LA TABLA USUARIO...................................................
		  public String[] getValuesUsuario (String usuario,String clave)
		  {
		        String cadena[] = new String[9];
		        
		        try {
		        	String Query = "SELECT * FROM " + "USUARIOS" + " WHERE Usuario = '" + usuario+ "'"+" and Clave = '" + clave+ "'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		        
		            while (resultSet.next()) {
		            	
		            	cadena[0]=resultSet.getString("USUARIO");
		            	cadena[1]=resultSet.getString("CLAVE");
		            	cadena[2]=resultSet.getString("NOMBRE");
		            	cadena[3]=resultSet.getString("APELLIDO");
		            	cadena[4]=resultSet.getString("EMAIL");
		            	cadena[5]=resultSet.getString("CODIGO_POSTAL");
		            	cadena[6]=resultSet.getString("ULTIMO_VIAJE");
		            	cadena[7]=resultSet.getString("PAIS_DESTINO");
		            	cadena[8]=resultSet.getString("OTROS_DATOS");
		            	
		                
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return cadena;
		    }	
//........---------------------------------METODO OBTENER VALORES DE LA TABLA USUARIO  para iniciar sesion -------
		 
		  public boolean getValuesUsuario_Sesion(String usuario, String clave) {
				 
			  	boolean estado=false;
		        try {
			           String Query = "SELECT * FROM " + "USUARIOS" + " WHERE Usuario = '" + usuario+ "'"+" and Clave = '" + clave+ "'";
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
				    if (resultSet.next()) {
				    	estado=true;
				     
				    }
				    else
				    {
				      estado=false;	
				    }
				   

		        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        return estado;
			    }
		
		  
		  
		  //piases
		  
		  public void insertDataPaises(String NOMBRE, String CAPITAL,String MONEDA, String IDIOMA, String BANDERA, int HABITANTES, String CONTINENTE, String Dni_Admin) {
			    try {
			        String Query = "INSERT INTO " + "PAISES" + " VALUES("
			            	+ "'"+ NOMBRE + "',"
			                + "'"+ MONEDA + "',"
			            	+ "'"+ IDIOMA + "',"
			            	+ "'"+ BANDERA + "',"
			            	+ "'"+ CAPITAL + "',"
			            	+ "'"+ HABITANTES + "',"
			            	+ "'"+ CONTINENTE + "',"
			            	+ "'"+ Dni_Admin + "')";
			        
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			   } catch (SQLException ex) {
			       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			   }
		   }
		//	 ciudad     	+ "SEQ_Ciudades.NEXTVAL"+","
		  public void insertDataCiudadesPueblos( String NOMBRE, String PAIS,String DESCRIPCION,String Dni_Admin) {
			     System.out.println("upa"+DESCRIPCION+" "+NOMBRE+" "+Dni_Admin+" "+PAIS);
			  try {
			        String Query = "INSERT INTO " + "CIUDADES_PUEBLOS" + " VALUES("
			            	+ "SEQ_Ciudades.NEXTVAL"+","
			                + "'"+ NOMBRE + "',"
			                + "'"+ DESCRIPCION + "',"
			            	+ "'"+ PAIS + "',"	
			         	    + "'"+ Dni_Admin + "')";
			       Statement st = Conexion.createStatement();
			       st.executeUpdate(Query);
			       JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			       
			       System.out.println("upa"+DESCRIPCION);
		   } catch (SQLException ex) {
		       JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		   }
	   } 
		  
		  
	//METODO QUE OBTIENE DATOS DEL PAIS SELECCIONADO		
		  public String []  getValues_PaisSeleccionado(String pais) {
			    String DatosPais[]=new String[20];
		        int cont=0; 
		        int tamany;
		        try {
		            String Query = "SELECT * FROM " + "PAISES"+ " WHERE NOMBRE = '" + pais + "'";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		            	
		            	DatosPais[cont]=resultSet.getString("Dni");
		            	cont++;
		                
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        return DatosPais;
		    }
		  
		  
  // obtener el nombre de los usuarios de los administradores para saber quien registro un pais pais determinado
		  
		  
		  public String[] getValues_NombreAdministrador()
		  {
		        String num[]=new String[20];
		        int cont=0; 
		        int tamany;
		        try {
		            String Query = "SELECT * FROM " + "Administradores";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		            
		            //tamany=resultSet.getRow();
		            while (resultSet.next()) {
		            	
		    	        num[cont]=resultSet.getString("Dni");
		            	cont++;

		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return num;
		    }
		  
		  
		  
		  public String[] getValuesPais ()
		  {
		        String num[]=new String[20];
		        int cont=0; 
		        int tamany;
		        try {
		            String Query = "SELECT * FROM " + "PAISES";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);
		       
		            while (resultSet.next()) {
		            	
		    	        num[cont]=resultSet.getString("NOMBRE");
		            	cont++;
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		        
		        return num;
		    }
	// obtener datos de ciudad
			 public void getValuesCiudad(String ciudad, String pais) {
				 

			        try {
				           String Query = "SELECT * FROM " + "CIUDADES_PUEBLOS" + " WHERE NOMBRE = '" + ciudad + "'"+" and NOMBRE_PAIS = '" + pais+ "'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					            System.out.println(ciudad);
			            while (resultSet.next()) {
				                System.out.println("Nombre: " + resultSet.getString("ID_CIUDAD") + " "
				                + "Descripcion: " + resultSet.getString("NOMBRE") + " " 
		                        + "Descripcion: " + resultSet.getString("NOMBRE_PAIS") + " " 
		                        + "pais: " + resultSet.getString("DESCRIPCION"));
			            }

			        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
				    }
			 

		 
	//METODO QUE ELIMINA el pais seleccionado 	
		 public void deleteRecord( String nombre) {
		        try {
		            String Query = "DELETE FROM " + "PAISES" + " WHERE NOMBRE = '" + nombre+ "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		 }
		      

}
