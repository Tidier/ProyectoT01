//Assiya Mrabet
package Ciudad;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Hoteles.MostrarHotel_App;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gestion_Ciudad_App extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gestion_Ciudad_App frame = new Gestion_Ciudad_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gestion_Ciudad_App() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 513, 401);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("A\u00F1adir");
	
		button.setFont(new Font("Arial", Font.BOLD, 11));
		button.setBounds(92, 163, 139, 54);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Editar");
		button_1.setFont(new Font("Arial", Font.BOLD, 11));
	
		button_1.setBounds(262, 163, 139, 54);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Eliminar");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_2.setFont(new Font("Arial", Font.BOLD, 11));
		button_2.setBounds(92, 247, 139, 54);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("Mostrar");
	
		button_3.setFont(new Font("Arial", Font.BOLD, 11));
		button_3.setBounds(262, 247, 139, 54);
		contentPane.add(button_3);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 25, 303, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(Gestion_Ciudad_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(313, 13, 148, 76);
		contentPane.add(label_1);
		
		JLabel lblGestinDeLa = new JLabel("Gesti\u00F3n de la ciudad");
		lblGestinDeLa.setFont(new Font("Arial", Font.BOLD, 12));
		lblGestinDeLa.setBounds(49, 122, 139, 14);
		contentPane.add(lblGestinDeLa);
		
//..................................llamar a la ventana de A�adir Ciudad.................................................
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false); //cerrar la ventana de gestion de ciudad
				A�adirCiudad ciudad=new A�adirCiudad();
				ciudad.setVisible(true);
			}
		});
//........................................FIN evento llamar a la ventana de a�adir ciduad...................................

//..................................llamar a la ventana de editar Ciudad.................................................

		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			setVisible(false); //cerrar la ventana de gestion de ciudad

			EditarCiudad_App editarciudad=new EditarCiudad_App();
			editarciudad.setVisible(true);
			}
		});

	//........................................FIN evento llamar a la ventana de editar ciduad...................................
	
	
	//..................................llamar a la ventana de eliminar Ciudad.................................................

		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false); //cerrar la ventana de gestion de ciudad

				EliminarCiudad_App ciudadBorrar=new EliminarCiudad_App();
				ciudadBorrar.setVisible(true);
			}
			
		});
		
		//........................................FIN evento llamar a la ventana de eliminar ciduad...................................

		//..................................llamar a la ventana de mostrar Ciudad.................................................
		
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false); //cerrar la ventana de gestion de ciudad

				MostrarCiudad_App mostrar=new MostrarCiudad_App();
				mostrar.setVisible(true);
			}
		});
		
	//........................................FIN evento llamar a la ventana de mostrar ciduad...................................

	}
}
