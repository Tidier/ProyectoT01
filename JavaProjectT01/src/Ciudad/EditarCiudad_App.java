package Ciudad;


import java.awt.BorderLayout;




import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;

public class EditarCiudad_App extends JFrame {

	private JPanel contentPane;
	private JTextField Nombre;
	private JTextField Pais;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditarCiudad_App frame = new EditarCiudad_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public EditarCiudad_App() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 549, 591);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 27, 335, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(EditarCiudad_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(347, 16, 158, 76);
		contentPane.add(label_1);
		
		final JLabel lblNewLabel = new JLabel("Datos de ciudad");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(34, 247, 114, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		
		final JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(34, 289, 57, 14);
		contentPane.add(lblNombre);
		lblNombre.setVisible(false);
		lblNewLabel.setVisible(false);

		
		Nombre = new JTextField();
		Nombre.setBounds(131, 286, 158, 20);
		contentPane.add(Nombre);
		Nombre.setColumns(10);
		Nombre.setVisible(false);
		lblNombre.setVisible(false);
		
		final JLabel lblNewLabel_1 = new JLabel("Pais");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(34, 317, 46, 14);
		contentPane.add(lblNewLabel_1);
		lblNewLabel_1.setVisible(false);
		
		
		final JLabel lblDescripcion = new JLabel("Descripci\u00F3n");
		lblDescripcion.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcion.setBounds(33, 342, 69, 14);
		contentPane.add(lblDescripcion);
		lblDescripcion.setVisible(false);
	
		
		JButton btnGuardar = new JButton("Guardar");
		
		
		btnGuardar.setFont(new Font("Arial", Font.BOLD, 11));
		btnGuardar.setBounds(387, 496, 89, 23);
		contentPane.add(btnGuardar);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(EditarCiudad_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(34, 466, 68, 53);
		contentPane.add(btnAtras);
		
		
		
		final TextArea descripcion = new TextArea();
		descripcion.setBounds(131, 343, 273, 114);
		contentPane.add(descripcion);
		descripcion.setVisible(false);
;
		
		
		final JLabel NombreCadena = new JLabel("Valor debe de ser una cadena");
		NombreCadena.setForeground(Color.RED);
		NombreCadena.setFont(new Font("Arial", Font.BOLD, 11));
		NombreCadena.setBounds(307, 286, 169, 20);
		contentPane.add(NombreCadena);
		NombreCadena.setVisible(false);
		
		Pais = new JTextField();
		Pais.setEditable(false);
		Pais.setColumns(10);
		Pais.setBounds(131, 317, 158, 20);
		contentPane.add(Pais);
		Pais.setVisible(false);
		
		JLabel lblCiudadQueQuieres = new JLabel("Ciudad que quieres ");
		lblCiudadQueQuieres.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudadQueQuieres.setBounds(37, 143, 111, 14);
		contentPane.add(lblCiudadQueQuieres);
		
		final JComboBox ListaCiudad = new JComboBox();
		ListaCiudad.setBounds(168, 140, 140, 20);
		contentPane.add(ListaCiudad);
	
//..........................codigo para obtener ciudades de BD y a�adirlos a combox...........................................//

		SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesCiudad();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaCiudad.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}

//..........................fin de codigo para obtener CIUDADES y a�adirlos a combox...........................................//
		
		
		JLabel lblNewLabel_3 = new JLabel("  editar");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_3.setBounds(59, 157, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
	
		btnNewButton.setBounds(352, 139, 89, 23);
		contentPane.add(btnNewButton);
		
		final JLabel Admin = new JLabel("Administrador");
		Admin.setFont(new Font("Arial", Font.BOLD, 11));
		Admin.setBounds(236, 219, 89, 14);
		contentPane.add(Admin);
		Admin.setVisible(false);
		
		final JComboBox ListaAdmin = new JComboBox();
		ListaAdmin.setBounds(347, 216, 129, 20);
		contentPane.add(ListaAdmin);
		
		final JComboBox ListaPais = new JComboBox();
		ListaPais.setBounds(335, 314, 130, 20);
		contentPane.add(ListaPais);
		ListaAdmin.setVisible(false);
		ListaPais.setVisible(false);

	//......................obtener los nombre de los admin. para identificar quien admin regisro la ciudad....................
		
			//SQL bdAdmin=new SQL();
			//try {
				//bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValues_usuaroAdministrador();
						  
				
				for(int i=0;i<rellenar.length;i++)
				{
					ListaAdmin.addItem(rellenar[i]);
					
				}
				bd.closeConnection();
			/*} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}*/
	//-------------------------------------------------------FIN CODIGO DE OBTENER ADMIN.........................................		
		
		//........................................ codigo para obtener todos los paises de BD.................................
				try {
					bd.SQLConnection("grupo1","root","");
					String rellenar2[]=bd.getValuesPais();
				
						for(int i=0;i<rellenar2.length;i++)
					{
					   ListaPais.addItem(rellenar2[i]);		
					}
					//bd.closeConnection();
			    } catch (Exception e2) {
				// TODO Auto-generated catch block
			    	e2.printStackTrace();
			    }
				
		//........................................ FIN codigo para obtener todos los paises de BD.................................
	
		
//.................................................... evento a clicar sobre label,atras................................//
		
				 MouseListener clikLabel=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
					 
					 setVisible(false);  //CERRAMOS LA VENTANA DE editar ciudad

					   Gestion_Ciudad_App atras=new Gestion_Ciudad_App();
					   atras.setVisible(true);				   }
				};
				 btnAtras .addMouseListener(clikLabel);
				 
//...........................................fin codigo atras..........................................................//
				 
//.................................... evento para mostrar datos de la ciudad selecionada......................................	
					
				 btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
								
							//SQL bd=new SQL();
							 
							 try {
								 String ciudad= ListaCiudad.getSelectedItem().toString();
								 String QueryPais="SELECT * FROM " + "CIUDADES_PUEBLOS"+ " WHERE NOMBRE= '"+ciudad+"'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									 Nombre.setText(ResultSet.getString("Nombre"));
									 Pais.setText(ResultSet.getString("Nombre_Pais"));
									 descripcion.setText(ResultSet.getString("Descripcion"));

								 }
								 
		
							// hacer visible los campos para que el admin. modifique los datos de la ciudad selecionada
							descripcion.setVisible(true);
							lblDescripcion.setVisible(true);
							lblNewLabel_1.setVisible(true);
							Nombre.setVisible(true);
							Pais.setVisible(true);
							lblNombre.setVisible(true);
							lblNewLabel.setVisible(true);
							ListaAdmin.setVisible(true);
							Admin.setVisible(true);
							ListaPais.setVisible(true);


						
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							
							

						}
					});
//....................................fin  evento para mostrar datos de la ciudad selecionada......................................	
		
				 
//..................................... evento para guardar los datos cambios de la ciudad a BD..............................
				 btnGuardar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
						String dniAdmin=ListaAdmin.getSelectedItem().toString();
						String nom_ciudad= ListaCiudad.getSelectedItem().toString();
						String pais=ListaPais.getSelectedItem().toString();
						 
						SQL bd=new SQL();
						String nom=Nombre.getText();
					    String Descripcion=descripcion.getText();
						
						try {
							bd.SQLConnection("grupo1","root","");
							String dni=bd.Obtener_Dni_Admin(dniAdmin); //obtener el dni de admin
							int cod_ciudad=bd.ObtenerCod_Ciudad(nom_ciudad); // obtener el codigo de ciudad
							bd.UpdatedeleteRecord_Ciudad(nom, dni, pais, Descripcion,cod_ciudad);
							
							
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						}
					});

	bd.closeConnection();
				        
//..................................... FIN de evento para guardar los datos cambios de la ciudad a BD..............................
		        
					        
						
				
						
	
					
	}
}

