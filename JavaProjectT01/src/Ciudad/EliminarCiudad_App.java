package Ciudad;

import java.awt.BorderLayout;
import WipAssiya.SQL;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Pais.GestionPais;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;

public class EliminarCiudad_App extends JFrame {

	private JPanel contentPane;
	SQL bd=new SQL();

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarPaisApp frame = new EliminarPaisApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public EliminarCiudad_App() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 510, 427);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(Color.BLUE);
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(21, 64, 312, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(EliminarCiudad_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(335, 31, 149, 76);
		contentPane.add(label_1);
		
		JLabel EliminarCiudad = new JLabel("Eliminar la ciudad\r\n");
		EliminarCiudad.setFont(new Font("Arial", Font.BOLD, 12));
		EliminarCiudad.setBounds(102, 183, 134, 14);
		contentPane.add(EliminarCiudad);
		
		JLabel lblNewLabel_1 = new JLabel("Ciudad");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(140, 239, 52, 14);
		contentPane.add(lblNewLabel_1);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(215, 236, 139, 20);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("Eliminar");
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 11));
		
		btnNewButton.setBounds(254, 292, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(EliminarCiudad_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(59, 296, 68, 51);
		contentPane.add(btnAtras);
		
//....... codigo para a�adir paises k hay en BD en comboBox para que el admin. seleccione el pais que quiere eliminar.........
		
				
				
				try {
					bd.SQLConnection("grupo1","root","");
					String rellenar[]=bd.getValuesCiudad();
					
					for(int i=0;i<rellenar.length;i++)
					{
						comboBox.addItem(rellenar[i]);		
					}
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

//..............................fin de codigo de a�adir paises a combox.............................................................				
				
//.................................... borrar de base de datos ciudad seleccionada.............................................
				
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
					
						//SQL bd=new SQL();
						
						try {
						//	bd.SQLConnection("grupo1","root","");
				            String Ciudadseleccionado=comboBox.getSelectedItem().toString();
				            int cod=bd.ObtenerCod_Ciudad(Ciudadseleccionado);
					        bd.deleteRecord_Ciudad(cod,Ciudadseleccionado);
					        
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
				});

//............................................. fin de evento para eliminar pais selecionado de BD............................................//
	
//............................................. evento a clicar sobre label,atras............................................//
				
				 MouseListener clikLabel2=new MouseListener() {
						   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
					 
					   setVisible(false);  //CERRAMOS LA VENTANA DE ELIMINAR

					   Gestion_Ciudad_App atras=new  Gestion_Ciudad_App();
					   atras.setVisible(true);		
					 }
				};
				btnAtras.addMouseListener(clikLabel2);
				 
//............................................. fin evento a clicar sobre label,atras............................................//
	}
}
