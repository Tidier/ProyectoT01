/**
 * 
 */
package Main_Comun_App;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import com.sun.org.apache.xml.internal.security.transforms.implementations.TransformXSLT;

import Administrador.AdministradorVerUsuario;

/**
 * @author Miguel Angel
 *
 */
public class SQL {
	
	private static Connection Conexion = null;


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            //JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	            System.out.println("Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            //JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	            System.out.println("NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            //JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	            System.out.println("NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            //JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		            System.out.println("Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            //JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		            System.out.println("NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		public void createTable(String name) {
		        try {
		            String Query = "CREATE TABLE " + name + ""
		                    + "(ID VARCHAR(25),Nombre VARCHAR(50), Apellido VARCHAR(50),"
		                    + " Edad VARCHAR(3), Sexo VARCHAR(1))";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + name + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		        }
		    }
		

	 //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertData(String table_name, String ID, String name, String lastname, String age, String gender) {
		        try {
		            String Query = "INSERT INTO " + table_name + " VALUES("
		            		+ "'"+ ID + "',"
		            		+ "'"+ name + "',"
		            		+ "'"+ lastname + "',"
		            		+ "'"+ age + "',"
		            		+ "'"+ gender + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
		 
	      //METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		 (ORIGINAL)
		  public void getValues(String table_name) {
		        try {
		            String Query = "SELECT * FROM " + table_name;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("ID: " + resultSet.getString("ID") + " "
		                        + "Nombre: " + resultSet.getString("Nombre") + " " 
								+ resultSet.getString("Apellido") + " "
		                        + "Edad: " + resultSet.getString("Edad") + " "
		                        + "Sexo: " + resultSet.getString("Sexo"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
				   
		  //METODO OBTENER VALORES DE LA TABLA USUARIO
			  public String[] getValuesUsuario (String valor)
			  {

			        String cadena[] = new String[10];
			        
			        try {
			            String Query = "SELECT * FROM USUARIOS WHERE USUARIO='"+valor+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			       
			            while (resultSet.next()) {
			            	
			            	cadena[0]=resultSet.getString("USUARIO");
			            	cadena[1]=resultSet.getString("CLAVE");
			            	cadena[2]=resultSet.getString("NOMBRE");
			            	cadena[3]=resultSet.getString("APELLIDO");
			            	cadena[4]=resultSet.getString("EMAIL");
			            	cadena[5]=resultSet.getString("CODIGO_POSTAL");
			            	cadena[6]=resultSet.getString("ULTIMO_VIAJE");
			            	cadena[7]=resultSet.getString("PAIS_DESTINO");
			            	cadena[8]=resultSet.getString("ESTADO");
			            	cadena[9]=resultSet.getString("OTROS_DATOS");
			            	
			                
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			        
			        return cadena;
			    }		 	

			  //METODO OBTENER VALORES DE LA TABLA ADMINISTRADOR
			  public String[] getValuesAdministrador (String valor)
			  {

			        String cadena[] = new String[10];
			        
			        try {
			            String Query = "SELECT * FROM ADMINISTRADORES WHERE USUARIO='"+valor+"'";
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
			       
			            while (resultSet.next()) {
			            	
			            	cadena[0]=resultSet.getString("DNI");
			            	cadena[1]=resultSet.getString("CLAVE");
			            	cadena[2]=resultSet.getString("USUARIO");
			            	cadena[3]=resultSet.getString("NOMBRE");
			            	cadena[4]=resultSet.getString("APELLIDO");
			            	cadena[5]=resultSet.getString("DNI_SUPERUSUARIO");
			            	
			            				                
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			        
			        return cadena;
			    }
			  
		  //METODO PARA MODIFICAR DATOS DE LA TABLA USUARIO---------------------------------------------------------------------------------------------------------------
			 
			  public void modValuesUsuario (String nombre, String apellido, String email, int codPostal, String uViaje, String pDestino, String oDatos, String valor){
				  
			        try {
			   
			        		        	
			        	String Query = "UPDATE USUARIOS SET NOMBRE ='"+nombre+"',APELLIDO='"+apellido+"',EMAIL='"+email+"',CODIGO_POSTAL='"+codPostal+"'"
			        			+ ",ULTIMO_VIAJE='"+uViaje+"',PAIS_DESTINO='"+pDestino+"',OTROS_DATOS='"+oDatos+"' WHERE USUARIO ='"+valor+"'";
			        				            
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            
			            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");
			            System.out.println(nombre);
			           
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en modificaci�n de datos");
			        }			  		  
				  
			  }
			  
			  //METODO PARA MODIFICAR DATOS DE LA TABLA ADMINISTRADORES----------------------------------------------------------------------------------------------------
				 
			  public void modValuesAdministrador (String dni, String nombre, String apellido, String valor){
				  
			        try {
			   
			        		        	
			        	String Query = "UPDATE ADMINISTRADORES SET DNI ='"+dni+"', NOMBRE ='"+nombre+"',APELLIDO ='"+apellido+"' WHERE USUARIO ='"+valor+"'";
			        				            
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            
			            JOptionPane.showMessageDialog(null, "Se ha modificado los datos");
			            System.out.println(nombre);
			           
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en modificaci�n de datos");
			        }			  		  
				  
			  }
			  
			//..............................METODO OBTENER VALORES DE LA TABLA ADMINISTRADORES  para iniciar sesion .............................-----------------//		 
				
			  public boolean getValuesAdministrador_Clave(String clave) {
					 
				  	boolean estado=false;
			        try {
				           String Query = "SELECT * FROM " + "ADMINISTRADORES" + " WHERE CLAVE = '" + clave+ "'";
					            Statement st = Conexion.createStatement();
					            java.sql.ResultSet resultSet;
					            resultSet = st.executeQuery(Query);
					    if (resultSet.next()) {
					    	estado=true;
					     
					    }
					    else
					    {
					      estado=false;	
					    }
					   

			        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			        return estado;
				    }

			  //METODO CAMBIAR CONTRASE�A DE ADMINISTRADOR--------------------------------------------------------------------------------------------------
			  

			 public void CambiarContrase�a(String contraNueva,String contraActual,String valor)
			 {
				 try {
			           

			        	String Query = "UPDATE ADMINISTRADORES SET CLAVE ='"+contraNueva+"' WHERE CLAVE='"+contraActual+"' AND USUARIO='"+valor+"'";
			        	
			        	
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			          
			            
			            JOptionPane.showMessageDialog(null, "Se ha cambiado la contrase�a");


			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error a modificar el registro especificado");
			        }
			 }	 
				 
		//------------------------------------------------------------------------------------------------------------------------------------------------------	-	
			  		  
			  
	      //METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		    public void deleteRecord(String table_name, String ID) {
		        try {
		            String Query = "DELETE FROM " + table_name + " WHERE ID = '" + ID + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }

	

}
