
package WipAssiya;

import java.sql.Connection;


import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 * @author Assiya Mrabet
 *
 */
public class SQL_Assiya {
	
	private static Connection Conexion = null;
	String name="Opinar_Restaurante";
	String  nameBar="Baros";
	String NameTenerBar="Tener_Bar";
	String nameOpinarBar="Opinar_bar";
	String nameEntretenimiento="Entretenimiento";
	String nameTenerEntretenimiento="Tener_Entretenimiento";


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }

// Opinar restaurante
	//METODO QUE CREA TABLA Opinar_Restaurante EN NUESTRA BASE DE DATOS	
		public void createTable() {
		        try {
		        	
		            String Query = "CREATE TABLE " +name + ""
		                    + "(Id_restaurante int,Usuario VARCHAR2(20), Clave VARCHAR2(10),"
		                    + "Opinar VARCHAR2(300), data_comentario VARCHAR2(10))";

		            JOptionPane.showMessageDialog(null, "Se ha creado la base de tabla " + name + " de forma exitosa");
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex); 
		            JOptionPane.showMessageDialog(null, "error " + name);

		        }
		 
		}		

	 //METODO QUE INSERTA VALORES de la tabla Opinar_Restaurante EN NUESTRA BASE DE DATOS
		 public void insertData_OpinarRestaurante( int Id_restaurante, String Usuario, String Clave, String Opinar) {
		        try {
		        	
		            String Query = "INSERT INTO " + name + " VALUES("
		            		+ "'"+ Id_restaurante + "',"
		            		+ "'"+ Usuario + "',"
		            		+ "'"+ Clave + "',"
		            		+ "'"+ Opinar+ "',"
		            		+ "'"+ Fechapc()+ "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
		 
	//METODO QUE OBTIENE VALORES DE la tabla de Opinar_Restaurante de NUESTRA BASE DE DATOS		
		 public void getValues_opinarRestaurante() {
		        try {
		            String Query = "SELECT * FROM " + name;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("Id_restaurante: " + resultSet.getString("Id_restaurante") + " "
		                        + "Usuario: " + resultSet.getString("Usuario") + " " 
								+ "Clave: "+ resultSet.getString("Clave") + " "
		                        + "Opinar: " + resultSet.getString("Opinar") + " "
		                        + "data_comentario: " + resultSet.getString("data_comentario"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
		 
	//METODO QUE ELIMINA VALORES de la tabla Opinar_Restaurante DE NUESTRA BASE DE DATOS	
		 public void deleteRecord_OpinarRestaurante( int Id_restaurante) {
		        try {
		            String Query = "DELETE FROM " + name + " WHERE Id_restaurante = '" + Id_restaurante + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }

		 
// bares
		 
		//METODO QUE CREA TABLA de bares EN NUESTRA BASE DE DATOS	
			public void createTable_Bares() {
			        try {
			            String Query = "CREATE TABLE " + nameBar + ""
			                    + "(Id_bar int, Nombre VARCHAR2(100), contacto int, Especialidad varchar2(200),"
			                    + "Precio int, Horario VARCHAR2(50), Descripcion VARCHAR2(200))";

			            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " +  nameBar + " de forma exitosa");
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            
			        } catch (SQLException ex) {
			            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
			            JOptionPane.showMessageDialog(null, "error " + nameBar);
			        }
			    }
			
		 //METODO QUE INSERTA VALORES de bares EN NUESTRA BASE DE DATOS
			 public void insertData_Bares( int Id_bar, String Nombre, int contacto, String Especialidad,int precio,String Horario, String Descripcion) {
			        try {
			            String Query = "INSERT INTO " + nameBar + " VALUES("
			            		+ "'"+ Id_bar + "',"
			            		+ "'"+ Nombre + "',"
			            		+ "'"+ contacto + "',"
			            		+ "'"+ Especialidad+ "',"
			            		+ "'"+ precio+ "',"
			            		+ "'"+ Horario+ "',"
			            		+ "'"+ Descripcion+ "')";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);
			            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
			        }
			    }	
			 
			 
		//METODO QUE OBTIENE VALORES de bares DE NUESTRA BASE DE DATOS		
			 public void getValues_Bares() {
			        try {
			            String Query = "SELECT * FROM " + nameBar;
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);

			            while (resultSet.next()) {
			                System.out.println("Id_bar: " + resultSet.getString("Id_bar") + " "
			                        + "Nombre: " + resultSet.getString("Nombre") + " " 
									+ "contacto: "+ resultSet.getString("contacto") + " "
			                        + "Especialidad: " + resultSet.getString("Especialidad") + " "
			                        + "precio: " + resultSet.getString("precio") + " "
					                + "Horario: " + resultSet.getString("Horario") + " "
			                        + "Descripcion: " + resultSet.getString("Descripcion"));
			            }

			        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
			        }
			    }
			 
		//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
			 public void deleteRecord_Bares(int Id_bar) {
			        try {
			            String Query = "DELETE FROM " + nameBar+ " WHERE Id_bar = '" + Id_bar + "'";
			            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

			        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			        }
			    }

// Tener Bar
			 
//METODO QUE CREA TABLA de tener bares EN NUESTRA BASE DE DATOS	
	public void createTable_TenerBar() {
	     try {
	           String Query = "CREATE TABLE " + NameTenerBar + ""
		                    + "(Id_bar int,Id_ciudad int)";

	           JOptionPane.showMessageDialog(null, "Se ha creado la tabla " +  NameTenerBar + " de forma exitosa");
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            
		} catch (SQLException ex) {
		          Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
						
        }
    }
				
			

	 //METODO QUE INSERTA VALORES de tener bares EN NUESTRA BASE DE DATOS
	 public void insertData_TenerBares( int Id_bar, int Id_ciudad) {
	        try {
	            String Query = "INSERT INTO " + NameTenerBar + " VALUES("
		            		+ "'"+ Id_bar + "',"
		            		+ "'"+ Id_ciudad+ "')";
	            	Statement st = Conexion.createStatement();
	            	st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
				     JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
	   }	
				 
				 
//METODO QUE OBTIENE VALORES de tener bares DE NUESTRA BASE DE DATOS		
	public void getValues_TenerBares() {
	      try {
	           String Query = "SELECT * FROM " + NameTenerBar;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

	            while (resultSet.next()) {
	                System.out.println("Id_bar: " + resultSet.getString("Id_bar") + " "
	                + "Id_ciudad: " + resultSet.getString("Id_ciudad"));
	            }

	       } catch (SQLException ex) {
	    	   		JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos bar");
	  }
	   }
				 
		
//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
	 public void deleteRecord_TenerBares(int Id_bar, int Id_ciudad) {
        try {
            String Query = "DELETE FROM " + NameTenerBar+ " WHERE Id_bar = '" + Id_bar + "'"+" and Id_ciudad = '" + Id_ciudad+ "'";
            	Statement st = Conexion.createStatement();
				st.executeUpdate(Query);

	   } catch (SQLException ex) {
		        System.out.println(ex.getMessage());
				JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	  }
   }
			 

// Opinar bar
	//METODO QUE CREA TABLA Opinar_Restaurante EN NUESTRA BASE DE DATOS	
		public void createTable_OpinarBar() {
		 try {
						        	
	    String Query = "CREATE TABLE " + nameOpinarBar + ""
				      + "(Id_bar int,Usuario VARCHAR2(20), Clave VARCHAR2(10),"
					  + "Opinar VARCHAR2(300), data_comentario VARCHAR2(10))";

				JOptionPane.showMessageDialog(null, "Se ha creado la base de tabla " + nameOpinarBar + " de forma exitosa");
				Statement st = Conexion.createStatement();
				          st.executeUpdate(Query);
		} catch (SQLException ex) {
		         Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex); 
		         JOptionPane.showMessageDialog(null, "error " + nameOpinarBar );

				 }
						 
		}		

	 //METODO QUE INSERTA VALORES de la tabla Opinar_BAR EN NUESTRA BASE DE DATOS
	 public void insertData_OpinarBar( int Id_bar, String Usuario, String Clave, String Opinar) {
	       try {		        	
				 String Query = "INSERT INTO " + nameOpinarBar + " VALUES("
				        		+ "'"+ Id_bar + "',"
				           		+ "'"+ Usuario + "',"
				           		+ "'"+ Clave + "',"
				           		+ "'"+ Opinar+ "',"
				           		+ "'"+ Fechapc()+ "')";
				      Statement st = Conexion.createStatement();
				      st.executeUpdate(Query);
				      JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
	        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
	        }
    }	
				 
						 
	//METODO QUE OBTIENE VALORES DE la tabla de Opinar_Bar de NUESTRA BASE DE DATOS		
	 public void getValues_OpinarBar() {
	        try {
		           String Query = "SELECT * FROM " + nameOpinarBar;
			            Statement st = Conexion.createStatement();
			            java.sql.ResultSet resultSet;
			            resultSet = st.executeQuery(Query);
	            while (resultSet.next()) {
		                System.out.println("Id_bar: " + resultSet.getString("Id_Bar") + " "
                        + "Usuario: " + resultSet.getString("Usuario") + " " 
						+ "Clave: "+ resultSet.getString("Clave") + " "
                        + "Opinar: " + resultSet.getString("Opinar") + " "
                        + "data_comentario: " + resultSet.getString("data_comentario"));
	            }

	        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
	        }
		    }
						 
	//METODO QUE ELIMINA VALORES de la tabla Opinar_bar DE NUESTRA BASE DE DATOS	
		public void deleteRecord_OpinarBar( int Id_bar) {
		       try {
		            String Query = "DELETE FROM " + nameOpinarBar+ " WHERE Id_bar = '" + Id_bar+ "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		       } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		       }
		    }

//Entretenimiento
		//METODO QUE CREA TABLA Entretenimiento EN NUESTRA BASE DE DATOS	
      public void createTable_Entretenimiento() {
		   try {       	
				String Query = "CREATE TABLE " +nameEntretenimiento + ""
				             + "(Id_entretenimiento int,Tipo VARCHAR2(200), Descripcion VARCHAR2(200),"
				             + "Horario VARCHAR2(200), contacto int)";

				JOptionPane.showMessageDialog(null, "Se ha creado la base de tabla " + nameEntretenimiento + " de forma exitosa");
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				} catch (SQLException ex) {
				            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex); 
				            JOptionPane.showMessageDialog(null, "error " + nameEntretenimiento );

				}
				}		

			 //METODO QUE INSERTA VALORES de la tabla Opinar_Restaurante EN NUESTRA BASE DE DATOS
				 public void insertData_Entretenimiento ( int Id_Entretenimiento , String tipo, String Descripcion, String horario, int contacto) {
				        try {
				        	
				            String Query = "INSERT INTO " + nameEntretenimiento  + " VALUES("
				            		+ "'"+ Id_Entretenimiento  + "',"
				            		+ "'"+ tipo + "',"
				            		+ "'"+ Descripcion+ "',"
				            		+ "'"+ horario+ "',"
				            		+ "'"+ contacto+ "')";
				            Statement st = Conexion.createStatement();
				            st.executeUpdate(Query);
				            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
				        } catch (SQLException ex) {
				            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
				        }
				    }	
				 
				 
//METODO QUE OBTIENE VALORES DE la tabla de Opinar_Restaurante de NUESTRA BASE DE DATOS		
	 public void getValues_Entretenimiento () {
		        try {
			           String Query = "SELECT * FROM " + nameEntretenimiento;
				            Statement st = Conexion.createStatement();
				            java.sql.ResultSet resultSet;
				            resultSet = st.executeQuery(Query);
		            while (resultSet.next()) {
			                System.out.println("Id_Entretenimiento: " + resultSet.getString("Id_Entretenimiento") + " "
	                        + "Tipo: " + resultSet.getString("Tipo") + " " 
							+ "Descripcion: "+ resultSet.getString("Descripcion") + " "
	                        + "Horario: " + resultSet.getString("Horario") + " "
	                        + "Contacto: " + resultSet.getString("contacto"));
		            }

		        } catch (SQLException ex) {
			            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
			    }
				
//METODO QUE ELIMINA VALORES de la tabla Opinar_Restaurante DE NUESTRA BASE DE DATOS	
	 public void deleteRecord_Entretenimiento ( int Id_entretenimiento ) {
	        try {
	            String Query = "DELETE FROM " + nameEntretenimiento  + " WHERE Id_entretenimiento  = '" + Id_entretenimiento + "'";
     		            Statement st = Conexion.createStatement();
			            st.executeUpdate(Query);

	        } catch (SQLException ex) {
			            System.out.println(ex.getMessage());
			            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
	        }
		    }
		

	 

	 
// Tener_Entretenimiento
				 
	//METODO QUE CREA TABLA de Tener_Entretenimiento  EN NUESTRA BASE DE DATOS	
		public void createTable_TenerEntretenimiento() {
		      try {
		           String Query = "CREATE TABLE " + nameTenerEntretenimiento+ ""
			                    + "(Id_entretenimiento int,Id_ciudad int)";

		           JOptionPane.showMessageDialog(null, "Se ha creado la tabla " +  nameTenerEntretenimiento + " de forma exitosa");
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
					            
		      } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
							
	        }
		    }
					
				
//METODO QUE INSERTA VALORESde la tabla Tener Entretenimiento EN NUESTRA BASE DE DATOS
					 public void insertData_TenerEntretenimiento( int Id_entretenimiento, int Id_ciudad) {
			        try {
					            String Query = "INSERT INTO " + nameTenerEntretenimiento + " VALUES("
					            		+ "'"+ Id_entretenimiento + "',"
					            		+ "'"+ Id_ciudad+ "')";
					            Statement st = Conexion.createStatement();
					            st.executeUpdate(Query);
					            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
					        } catch (SQLException ex) {
					            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
					        }
					    }	
					 
					 
	//METODO QUE OBTIENE VALORES de la tabla Tener EntretenimientoDE NUESTRA BASE DE DATOS		
	 public void getValues_TenerEntretenimiento() {
	       try {
	            String Query = "SELECT * FROM " + nameTenerEntretenimiento;
	            	Statement st = Conexion.createStatement();
					java.sql.ResultSet resultSet;
					resultSet = st.executeQuery(Query);

					while (resultSet.next()) {
					       System.out.println("Id_Entretenimiento: " + resultSet.getString("Id_entretenimiento") + " "
					       + "Id_ciudad: " + resultSet.getString("Id_ciudad"));
					            }

					} catch (SQLException ex) {
					        JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
	    }
					 
			
	//METODO QUE ELIMINA VALORES  de la tabla Tener Entretenimiento DE NUESTRA BASE DE DATOS	
	 public void deleteRecord_TenerEntretenimiento(int Id_entretenimiento, int Id_ciudad) {
	        try {
	            String Query = "DELETE FROM " + nameTenerEntretenimiento+ " WHERE Id_entretenimiento = '" + Id_entretenimiento + "'"+" and Id_ciudad = '" + Id_ciudad+ "'";
	            	Statement st = Conexion.createStatement();
					st.executeUpdate(Query);

		    } catch (SQLException ex) {
		    		System.out.println(ex.getMessage());
					JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
			}
		}
		
// metodo para obtener la fecha del pc
	public String Fechapc()
	{
		Calendar c;
		c=Calendar.getInstance();
		int d=c.get(Calendar.DATE), m=1+(c.get(Calendar.MONTH)), a=c.get(Calendar.YEAR);
		
		String fecha=d+"/"+m+"/"+a;
		return fecha;
	}
	

}
