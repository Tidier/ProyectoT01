package Pais;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WipAssiya.SQL;
import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;

public class Tablapais extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField capital;
	private JTextField habitantes;
	private JTextField idioma;
	private JTextField moneda;
	private JTextArea ColorBandera; 
	SQL bd=new SQL();

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tablapais frame = new Tablapais();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Tablapais() {
		setTitle("Pais");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 581, 619);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(71, 47, 364, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Tablapais.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(402, 29, 143, 76);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Datos del pais");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(38, 222, 97, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel_1.setBounds(38, 284, 67, 14);
		contentPane.add(lblNewLabel_1);
		
		nombre = new JTextField();
		nombre.setBounds(139, 272, 121, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		JLabel lblHabitantes = new JLabel("Habitantes");
		lblHabitantes.setFont(new Font("Arial", Font.BOLD, 11));
		lblHabitantes.setBounds(38, 362, 67, 14);
		contentPane.add(lblHabitantes);
		
		capital = new JTextField();
		capital.setColumns(10);
		capital.setBounds(139, 303, 121, 20);
		contentPane.add(capital);
		
		habitantes = new JTextField();
		habitantes.setFont(new Font("Arial", Font.BOLD, 11));
		habitantes.setColumns(10);
		habitantes.setBounds(139, 359, 121, 20);
		contentPane.add(habitantes);
		
		JLabel lblContinente = new JLabel("Continente");
		lblContinente.setFont(new Font("Arial", Font.BOLD, 11));
		lblContinente.setBounds(38, 337, 63, 14);
		contentPane.add(lblContinente);
		
		JLabel label_2 = new JLabel("Idioma");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(38, 387, 67, 14);
		contentPane.add(label_2);
		
		idioma = new JTextField();
		idioma.setColumns(10);
		idioma.setBounds(139, 384, 121, 20);
		contentPane.add(idioma);
		
		JLabel lblCapital = new JLabel("Capital");
		lblCapital.setFont(new Font("Arial", Font.BOLD, 11));
		lblCapital.setBounds(38, 309, 67, 14);
		contentPane.add(lblCapital);
		
		JLabel lblMoneda = new JLabel("Moneda");
		lblMoneda.setFont(new Font("Arial", Font.BOLD, 11));
		lblMoneda.setBounds(38, 418, 67, 14);
		contentPane.add(lblMoneda);
		
		moneda = new JTextField();
		moneda.setColumns(10);
		moneda.setBounds(139, 415, 121, 20);
		contentPane.add(moneda);
		
		JLabel lblBandera = new JLabel("Colores de la ");
		lblBandera.setFont(new Font("Arial", Font.BOLD, 11));
		lblBandera.setBounds(38, 451, 78, 14);
		contentPane.add(lblBandera);
		
		JButton btnNewButton_1 = new JButton("Guardar");
	
		btnNewButton_1.setBounds(425, 514, 89, 23);
		contentPane.add(btnNewButton_1);
		
		//rellenamos JComboBox con los continentes, ya que son invariables
		final JComboBox continente = new JComboBox();
		continente.setBounds(139, 328, 121, 20);
		contentPane.add(continente);
		continente.addItem("�frica");
		continente.addItem("Am�rica");
		continente.addItem("Ant�rtida");
		continente.addItem("Asia");
		continente.addItem("Europa");
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(Tablapais.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(38, 522, 68, 48);
		contentPane.add(btnAtras);
		
		final JComboBox ListaAdmin = new JComboBox();
		ListaAdmin.setBounds(384, 189, 121, 20);
		contentPane.add(ListaAdmin);
///......................obtener los nombre de los admin. para identificar quien admin regisro el hotel....................
	
		try {
			bd.SQLConnection("grupo1","root","");
			
		String rellenarAdmin[]=bd.getValues_usuaroAdministrador();
		  
		
		for(int i=0;i<rellenarAdmin.length;i++)
		{
			ListaAdmin.addItem(rellenarAdmin[i]);
			
		}
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//-------------------------------------------------------FIN CODIGO DE OBTENER admin.........................................		
		JLabel lblAdministrador = new JLabel("Administrador");
		lblAdministrador.setFont(new Font("Arial", Font.BOLD, 11));
		lblAdministrador.setBounds(291, 192, 97, 14);
		contentPane.add(lblAdministrador);
		
		final JLabel ValorHabitants = new JLabel("El valor debe ser num\u00E9rico");
		ValorHabitants.setForeground(Color.RED);
		ValorHabitants.setFont(new Font("Arial", Font.BOLD, 12));
		ValorHabitants.setBounds(270, 362, 165, 14);
		contentPane.add(ValorHabitants);
		ValorHabitants.setVisible(false);

		final JLabel DatosObligatorios = new JLabel("Todos los datos son obligatorios");
		DatosObligatorios.setForeground(Color.RED);
		DatosObligatorios.setFont(new Font("Arial", Font.BOLD, 12));
		DatosObligatorios.setBounds(140, 517, 232, 14);
		contentPane.add(DatosObligatorios);
		DatosObligatorios.setVisible(false);
		
		final JLabel DatosCadena = new JLabel(" Nombre, Capital, Idioma y Moneda deben de ser una cadena");
		DatosCadena.setForeground(Color.RED);
		DatosCadena.setFont(new Font("Arial", Font.BOLD, 11));
		DatosCadena.setBounds(71, 247, 347, 14);
		contentPane.add(DatosCadena);
		
		final JTextArea ColorBandera = new JTextArea();
		ColorBandera.setBounds(139, 446, 121, 60);
		contentPane.add(ColorBandera);
		
		JLabel lblBandera_1 = new JLabel("      bandera");
		lblBandera_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblBandera_1.setBounds(38, 466, 78, 14);
		contentPane.add(lblBandera_1);
		DatosCadena.setVisible(false);
		
			
		
		
		
//..................................................evento para guardar los datos...........................................//
		
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
	           String seleccionado=(String)continente.getSelectedItem();
	           String dni=bd.Obtener_Dni_Admin(ListaAdmin.getSelectedItem().toString()); //obtener el dni de admin
	            
	            

	            SQL bd=new SQL();
				
				try {
					bd.SQLConnection("grupo1", "root", "");	
					DatosCadena.setVisible(false);
					
					if(nombre.getText().equalsIgnoreCase("")  || capital.getText().equalsIgnoreCase("") 
					   || moneda.getText().equalsIgnoreCase("") || idioma.getText().equalsIgnoreCase("")
					   || habitantes.getText().equalsIgnoreCase("") ) // comprobamos k han introducido dades 
					{
						DatosObligatorios.setVisible(true); // si no introducen todos los datos se muestra este mensaje

					} else{
						DatosObligatorios.setVisible(false);
						//comprobamos que los valores son cadenas de los campos(nombre,capital,idioma,moneda) y no numeros de
					  if(isCadena(nombre.getText(),capital.getText(),moneda.getText(),idioma.getText()))
					  {
						  DatosCadena.setVisible(true);
					  }else
					  {
						  
				    if (isNumeric(habitantes.getText())) //metode para comprobar si el valor de habitantes es numerico
				    {	
				        int habitant=Integer.parseInt(habitantes.getText());
				       
						bd.insertDataPaises(nombre.getText(),moneda.getText(),idioma.getText(),ColorBandera.getText(),capital.getText(),habitant,seleccionado,dni);
					    bd.closeConnection();
					  //  System.out.println("....... "+nombre.getText()+" "+capital.getText()+" "+moneda.getText()+" "+idioma.getText()+" "+imagen.getText()+" "+habitant+" "+seleccionado+" "+Administrador);
					} else{
						ValorHabitants.setVisible(true);// si no introdcuen un valor numerico en casilla de habitantes si muestra este mensaje
					    
					}
					  }  
				}

					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					
					

				}
			}
		});
		
		
//..................................................fin evento guardar.................................................//	
	
//................................................ evento a clicar sobre label,atras.......................................//
			
			 MouseListener clikLabel2=new MouseListener() {
					   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				setVisible(false);  //CERRAMOS LA VENTANA DE A�ADIR PAIS

				 GestionPais atras=new  GestionPais();
				   atras.setVisible(true);				   }
			};
			 btnAtras.addMouseListener(clikLabel2);
			 
//.................................................fin evento atras.....................................................................//		
			 

}


//....................................... metodo para comprobar si una cadena es un numero o letra...........................//

private static boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}

//..................................................fin evento comrobar cadena---------------------------------------//

//.........metodo comprobamos que los valores son cadenas de los campos(nombre,capital,idioma,moneda) y no numeros.........

private static boolean isCadena(String Nombre,String Capital, String Idioma, String Moneda){
	try {
		Integer.parseInt(Nombre);
		Integer.parseInt(Capital);
		Integer.parseInt(Idioma);
		Integer.parseInt(Moneda);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}

//...................................................fin medoto de comprobar valores...............................//
}
