package Pais;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionPais extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionPais frame = new GestionPais();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionPais() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 512, 416);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(17, 33, 303, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(GestionPais.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(330, 34, 143, 76);
		contentPane.add(label_1);
		
		JButton button = new JButton("A\u00F1adir");
		
		button.setFont(new Font("Arial", Font.BOLD, 11));
		button.setBounds(100, 187, 117, 49);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Editar");
	
		button_1.setFont(new Font("Arial", Font.BOLD, 11));
		button_1.setBounds(244, 186, 115, 50);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Eliminar");
		
		button_2.setFont(new Font("Arial", Font.BOLD, 11));
		button_2.setBounds(101, 245, 114, 46);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("Mostrar");
		
		button_3.setFont(new Font("Arial", Font.BOLD, 11));
		button_3.setBounds(243, 245, 118, 46);
		contentPane.add(button_3);
		
		JLabel lblGestinDelPais = new JLabel("Gesti\u00F3n del pais");
		lblGestinDelPais.setFont(new Font("Arial", Font.BOLD, 12));
		lblGestinDelPais.setBounds(50, 128, 116, 14);
		contentPane.add(lblGestinDelPais);
		
		
//...................................llamar a la ventana de A�adir pais...................................................
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			Tablapais pais=new Tablapais();
			pais.setVisible(true);
			}
		});
// ...............................................fin llamada a laventana de a�adir.........................................//
		
//...................................llamar a la ventana de eliminar pais...................................................
		
		
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EliminarPaisApp eliminarPais=new EliminarPaisApp();
				eliminarPais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de A�adir pais...................................................
				
//...........................................llamar a la ventana a eliminar pais.....................................................
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Editar_Pais_App editarpais=new Editar_Pais_App();
				editarpais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de eliminar pais...................................................
	
//....................................................llamar a la ventana para mostrar pais...........................//
		
		button_3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		 
			Mostrar_Paises_App mostrar=new Mostrar_Paises_App();
			mostrar.setVisible(true);
		
		}
		});
//...................................fin llamada a la ventana de mostrar pais...................................................

	}

}
