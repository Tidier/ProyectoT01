package Servicios;


import java.awt.BorderLayout;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import Pais.EliminarPaisApp;
import WipAssiya.SQL;
import Usuario.Comentarios_Usuario_APP;
import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JTextArea;

public class MostrarServiciosApp extends JFrame {

	private JPanel contentPane;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JTextField txtnombre;
	private JTextField txtpais;
	private JTextField txtciudad;
	private JTextField txtcontacto;
	int Id_Servicio=0;
	public String usuarios="";
	public String claves="";
	public String nameTable="Servicios";
	public String nombre="";
	public String table="Opinar_servicios";
	
	SQL bd=new SQL();
	private JTextField txtHorario;
	


	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MostrarHotel_App frame = new MostrarHotel_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MostrarServiciosApp(String usuario,String clave) {
		setResizable(false);
		
		this.usuarios=usuario;
		this.claves=clave;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 564, 610);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(38, 31, 335, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(MostrarServiciosApp.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(383, 11, 154, 76);
		contentPane.add(label_1);
;
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(328, 221, 124, 52);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblCiudadQueQuieres = new JLabel("Selecciona el servicio");
		lblCiudadQueQuieres.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudadQueQuieres.setBounds(37, 180, 148, 14);
		contentPane.add(lblCiudadQueQuieres);
		
		final JComboBox ListaServicio = new JComboBox();
		ListaServicio.setBounds(232, 177, 140, 20);
		contentPane.add(ListaServicio);
	
//..........................codigo para obtener hoteles de BD y a�adirlos a combox...........................................//

		//SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesServicios();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaServicio.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
	
		btnNewButton.setBounds(435, 176, 89, 23);
		contentPane.add(btnNewButton);
		
		final JLabel lblDatosDelHotel = new JLabel("Datos del servicio");
		lblDatosDelHotel.setFont(new Font("Arial", Font.BOLD, 12));
		lblDatosDelHotel.setBounds(38, 231, 120, 14);
		contentPane.add(lblDatosDelHotel);
		lblDatosDelHotel.setVisible(false);
		
		final JLabel lblNombre = new JLabel("Tipo");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(106, 273, 79, 14);
		contentPane.add(lblNombre);
		lblNombre.setVisible(false);
		
		final JLabel lblPais = new JLabel("Pais");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(106, 304, 68, 14);
		contentPane.add(lblPais);
		lblPais.setVisible(false);

		
		final JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudad.setBounds(106, 335, 68, 14);
		contentPane.add(lblCiudad);
		lblCiudad.setVisible(false);
		

		
		final JLabel lblContacto = new JLabel("Contacto");
		lblContacto.setFont(new Font("Arial", Font.BOLD, 11));
		lblContacto.setBounds(106, 366, 68, 14);
		contentPane.add(lblContacto);
		lblContacto.setVisible(false);

		
		final JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcin.setBounds(106, 430, 89, 14);
		contentPane.add(lblDescripcin);
		lblDescripcin.setVisible(false);

		
		txtnombre = new JTextField();
		txtnombre.setEditable(false);
		txtnombre.setBounds(212, 270, 179, 20);
		contentPane.add(txtnombre);
		txtnombre.setColumns(10);
		txtnombre.setVisible(false);
	
		
		txtpais = new JTextField();
		txtpais.setEditable(false);
		txtpais.setColumns(10);
		txtpais.setBounds(212, 301, 179, 20);
		contentPane.add(txtpais);
		txtpais.setVisible(false);
	
		
		txtciudad = new JTextField();
		txtciudad.setEditable(false);
		txtciudad.setColumns(10);
		txtciudad.setBounds(212, 332, 179, 20);
		contentPane.add(txtciudad);
		txtciudad.setVisible(false);

		
		txtcontacto = new JTextField();
		txtcontacto.setEditable(false);
		txtcontacto.setColumns(10);
		txtcontacto.setBounds(212, 363, 179, 20);
		contentPane.add(txtcontacto);
		txtcontacto.setVisible(false);

		
		final JTextArea txtdescripcion = new JTextArea();
		txtdescripcion.setEditable(false);
		txtdescripcion.setBounds(212, 425, 240, 76);
		contentPane.add(txtdescripcion);
		
		final JButton btnComentar = new JButton("Comentar");
		
		btnComentar.setFont(new Font("Arial", Font.BOLD, 11));
		btnComentar.setBounds(447, 533, 89, 23);
		contentPane.add(btnComentar);
		btnComentar.setVisible(false);
		
		final JLabel lblHorario = new JLabel("Horario");
		lblHorario.setFont(new Font("Arial", Font.BOLD, 11));
		lblHorario.setBounds(106, 397, 68, 14);
		contentPane.add(lblHorario);
		lblHorario.setVisible(false);
		
		
		txtHorario = new JTextField();
		txtHorario.setEditable(false);
		txtHorario.setBounds(212, 394, 179, 20);
		contentPane.add(txtHorario);
		txtHorario.setColumns(10);
		
		JLabel principal = new JLabel("P\u00E1gina principal ");
		principal.setForeground(Color.BLACK);
		principal.setFont(new Font("Arial", Font.BOLD, 13));
		principal.setBounds(28, 112, 124, 23);
		contentPane.add(principal);
		
		JLabel lblForum = new JLabel("      Forum");
		lblForum.setForeground(Color.BLACK);
		lblForum.setFont(new Font("Arial", Font.BOLD, 13));
		lblForum.setBounds(226, 112, 111, 23);
		contentPane.add(lblForum);
		
		JLabel iniciar = new JLabel("Iniciar sesion");
		iniciar.setForeground(Color.BLACK);
		iniciar.setFont(new Font("Arial", Font.BOLD, 13));
		iniciar.setBounds(413, 112, 111, 23);
		contentPane.add(iniciar);
		txtHorario.setVisible(false);
		txtdescripcion.setVisible(false);


	
	
		

				 
				 
//.................................... evento para mostrar datos de la ciudad selecionada......................................	
					
				 btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
								
							//SQL bd=new SQL();
						      //int Contacto=Integer.parseInt(txtContacto.getText()); // pasamos contacto a int

							 
							 try {
								 String Servicio=ListaServicio.getSelectedItem().toString();

								 String QueryPais="SELECT * FROM " + "SERVICIOS"+ " WHERE Tipo= '"+Servicio+"'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									txtnombre.setText(ResultSet.getString("Tipo"));
									txtcontacto.setText(ResultSet.getString("Contacto"));
									
									txtdescripcion.setText(ResultSet.getString("Descripcion"));
									
									txtHorario.setText(ResultSet.getString("Horario"));
									Id_Servicio=ResultSet.getInt("Id_Servicio");
									nombre=(ResultSet.getString("Tipo"));


								 }
								 
							 }catch (Exception e2) {
										// TODO Auto-generated catch block
										e2.printStackTrace();
									}
								 

								 try {
									 //String hotel= ListaServicio.getSelectedItem().toString();
										
										bd.SQLConnection("grupo1","root","");


									 String QueryPais="SELECT * FROM " + "Tener_servicios"+ " WHERE id_servicio= "+Id_Servicio;
									 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
									 Statement st=Conexion.createStatement();
									 java.sql.ResultSet ResultSet;
									 ResultSet= st.executeQuery(QueryPais);
									 
									 while (ResultSet.next())
									 {
										String  nom_ciudad=bd.getValues_NombreCiudad(ResultSet.getInt("id_ciudad"));
										 txtciudad.setText(nom_ciudad);
										txtpais.setText(ResultSet.getString("Nombre_Pais"));


									 }
							// hacer visible los campos para que el admin. modifique los datos de la ciudad selecionada
									txtdescripcion.setVisible(true);

									txtcontacto.setVisible(true);
									txtciudad.setVisible(true);
									txtpais.setVisible(true);
									txtnombre.setVisible(true);
									txtHorario.setVisible(true);
									lblDescripcin.setVisible(true);

									lblCiudad.setVisible(true);
									lblPais.setVisible(true);
									lblDatosDelHotel.setVisible(true);
									lblNombre.setVisible(true);
									lblContacto.setVisible(true);
									lblHorario.setVisible(true);
									btnComentar.setVisible(true);




						
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							
						

						}
					});
					bd.closeConnection();
//....................................fin  evento para mostrar datos de la ciudad selecionada......................................	

				
					
	//..................................... evento a clicar sobre label,atras............................................
					
					 MouseListener clikLabel=new MouseListener() {
							   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   GestionServicios atras=new GestionServicios();
						   atras.setVisible(true);				   }
					};
					 				
//..................................... FIN evento a clicar sobre label,atras............................................

					
//..................................... evento para ir a la pantalla de comentar............................................

					 btnComentar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
								Comentarios_Usuario_APP comenta=new Comentarios_Usuario_APP(usuarios,claves,nombre,nameTable,Id_Servicio,table);
								comenta.setVisible(true);
							//	comenta.Mostrar_Opiniones(Id_Hotel);
								setVisible(false);
							}
						});

	//..................................... FIN evento para ir a la pantalla de comentar............................................

					 //------------------------------------------------- Evento para ir al forum...............................................
						
					 MouseListener Forum=new MouseListener() {
						   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Forum_App PaginaForum=new  Forum_App(usuarios,claves);
							   PaginaForum.setVisible(true);				   }
						};
						lblForum.addMouseListener(Forum);
						    	
			//-------------------------------------------------FIN  Evento para ir al forum...............................................
	
						
						//------------------------------------pagina principal.................................................................
						 
						 MouseListener PaginaPrincipal=new MouseListener() {
								   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Pagina_Principal atras=new  Pagina_Principal();
							   atras.setVisible(true);				   }
						};
						principal.addMouseListener(PaginaPrincipal);
						    	
						   
				//------------------------------------fin ................................................................
						 
						//LISTENER DE LABEL INICIAR SESION
						MouseListener iniSesion =new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   Iniciar_Session_Usuario_App inicio = new Iniciar_Session_Usuario_App();
								   inicio.setVisible(true);
								   
								   }
							};
						iniciar.addMouseListener(iniSesion);				 
	}
}

