//Assiya Mrabet
package Usuario;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;

public class Recuperar_contrasenya_Usuario_App extends JFrame {

	private JPanel contentPane;
	private final JLabel label = new JLabel("Journey&Experience");
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Recuperar_contrasenya_Usuario_App frame = new Recuperar_contrasenya_Usuario_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Recuperar_contrasenya_Usuario_App() {
		setTitle("Recuperar contrase\u00F1a");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 588, 466);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		label.setBounds(60, 54, 329, 43);
		contentPane.add(label);
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Recuperar_contrasenya_Usuario_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(410, 11, 152, 99);
		contentPane.add(label_1);
		
		JLabel lblRecuperar = new JLabel("Recuperar contrase\u00F1a");
		lblRecuperar.setFont(new Font("Arial", Font.BOLD, 12));
		lblRecuperar.setBounds(74, 154, 177, 14);
		contentPane.add(lblRecuperar);
		
		JLabel lblEscribeTuCorreo = new JLabel("E-mail");
		lblEscribeTuCorreo.setFont(new Font("Arial", Font.PLAIN, 12));
		lblEscribeTuCorreo.setBounds(108, 250, 67, 14);
		contentPane.add(lblEscribeTuCorreo);
		
		textField = new JTextField();
		textField.setBounds(185, 248, 242, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Recuperar");
	
		btnNewButton.setFont(new Font("Arial", Font.PLAIN, 11));
		btnNewButton.setBounds(338, 318, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel.setBounds(109, 201, 60, 14);
		contentPane.add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(185, 199, 242, 20);
		contentPane.add(textField_1);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(Recuperar_contrasenya_Usuario_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(44, 317, 68, 51);
		contentPane.add(btnAtras);
		
		// evento a clicar sobre label,atras.
		
		 MouseListener clikLabel=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   Iniciar_Session_Usuario_App atras=new  Iniciar_Session_Usuario_App();
			   atras.setVisible(true);				   }
		};
		btnAtras.addMouseListener(clikLabel);
		//comprobar k el usuario y email existen en la BD, sino estan se mostrar un mensaje de error al usuario.
		// si esta en BD se enviara un correo al usuario con una contras. aleatoria o con su contr.
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
	}

}
