//Assiya Mrabet
package Usuario;
import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.JList;

public class EspacioPersonalApp extends JFrame {

	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JPanel contentPane;
	private JLabel lblNewLabel_1;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtEmail;
	private JTextField txtCodigoPostal;
	private JTextField txtPais;
	private JTextArea txtPviajes;
	private JTextArea txtOtrosDatos;
	private JButton btnEditar;
	private JLabel hola;
	public String usuarios="";
	public String claves="";


	private String nombre="";
	private JList listaHotel;
	//private String 

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EspacioPersonalApp frame = new EspacioPersonalApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public EspacioPersonalApp(String Usuario,String Clave) {
		
		this.usuarios=Usuario;
		this.claves=Clave;
		setResizable(false);
		setTitle("Espacio Personal");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 697, 724);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 30));
		label.setBounds(107, 41, 364, 43);
		contentPane.add(label);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.inactiveCaption);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.WHITE, null, null, null));
		panel.setBounds(10, 179, 421, 506);
		contentPane.add(panel);
		panel.setLayout(null);
		
		final JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(10, 11, 112, 71);
		lblNewLabel_1.setIcon(new ImageIcon(EspacioPersonalApp.class.getResource("/Vistas/logo.jpg")));
		panel.add(lblNewLabel_1);
		
		JLabel hola = new JLabel("Hola,");
		hola.setBounds(20, 93, 56, 14);
		hola.setFont(new Font("Arial", Font.BOLD, 11));
		panel.add(hola);
		
		JLabel modificar = new JLabel("Modificar datos\r\n");
		modificar.setBounds(307, 21, 82, 14);
		modificar.setForeground(new Color(0, 0, 0));
		modificar.setFont(new Font("Arial", Font.PLAIN, 11));
		panel.add(modificar);
		
		JLabel lblCambiarContrasea = new JLabel("Cambiar contrase\u00F1a");
		lblCambiarContrasea.setBounds(277, 68, 112, 14);
		lblCambiarContrasea.setForeground(new Color(0, 0, 0));
		lblCambiarContrasea.setFont(new Font("Arial", Font.PLAIN, 11));
		panel.add(lblCambiarContrasea);
		
		JLabel lblNewLabel_5 = new JLabel("Email ");
		lblNewLabel_5.setBounds(91, 195, 46, 14);
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_5.setBackground(Color.LIGHT_GRAY);
		panel.add(lblNewLabel_5);
		
		JLabel lblNombre = new JLabel("Nombre ");
		lblNombre.setBounds(91, 145, 62, 14);
		lblNombre.setFont(new Font("Arial", Font.BOLD, 12));
		lblNombre.setBackground(Color.LIGHT_GRAY);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido ");
		lblApellido.setBounds(91, 170, 62, 14);
		lblApellido.setFont(new Font("Arial", Font.BOLD, 12));
		lblApellido.setBackground(Color.LIGHT_GRAY);
		panel.add(lblApellido);
		
		JLabel lblCodigoPostal = new JLabel("Codigo postal ");
		lblCodigoPostal.setFont(new Font("Arial", Font.BOLD, 12));
		lblCodigoPostal.setBackground(Color.LIGHT_GRAY);
		lblCodigoPostal.setBounds(91, 223, 94, 14);
		panel.add(lblCodigoPostal);
		
		JLabel lblPasesDondeSueles = new JLabel(" Pa\u00EDses  donde sueles viajar \r\n");
		lblPasesDondeSueles.setFont(new Font("Arial", Font.BOLD, 12));
		lblPasesDondeSueles.setBackground(Color.LIGHT_GRAY);
		lblPasesDondeSueles.setBounds(91, 248, 165, 14);
		panel.add(lblPasesDondeSueles);
		
		JLabel lblSultimoViaje = new JLabel("Su \u00FAltimo viaje ");
		lblSultimoViaje.setFont(new Font("Arial", Font.BOLD, 12));
		lblSultimoViaje.setBackground(Color.LIGHT_GRAY);
		lblSultimoViaje.setBounds(93, 347, 92, 14);
		panel.add(lblSultimoViaje);
		
		JLabel lblOtrosDatos = new JLabel("Otros Datos");
		lblOtrosDatos.setFont(new Font("Arial", Font.BOLD, 12));
		lblOtrosDatos.setBackground(Color.LIGHT_GRAY);
		lblOtrosDatos.setBounds(93, 388, 92, 14);
		panel.add(lblOtrosDatos);
		
		final JTextArea txtPviajes = new JTextArea();
		txtPviajes.setEditable(false);
		txtPviajes.setBounds(91, 273, 240, 62);
		panel.add(txtPviajes);
		
//..........................FIN evento para ir al formulario de modificacion de datos..........................

//..................................... guardar los cambios k ha hecho el usuario en la base de datos
		
	
		
		final JTextArea txtOtrosDatos = new JTextArea();
		txtOtrosDatos.setEditable(false);
		txtOtrosDatos.setBounds(91, 413, 240, 37);
		panel.add(txtOtrosDatos);
		
		txtApellido = new JTextField();
		txtApellido.setEditable(false);
		txtApellido.setColumns(10);
		txtApellido.setBounds(198, 168, 130, 20);
		panel.add(txtApellido);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(198, 143, 130, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtCodigoPostal = new JTextField();
		txtCodigoPostal.setEditable(false);
		txtCodigoPostal.setColumns(10);
		txtCodigoPostal.setBounds(198, 221, 130, 20);
		panel.add(txtCodigoPostal);
		
		txtEmail = new JTextField();
		txtEmail.setEditable(false);
		txtEmail.setColumns(10);
		txtEmail.setBounds(198, 196, 130, 20);
		panel.add(txtEmail);
		
		txtPais = new JTextField();
		txtPais.setEditable(false);
		txtPais.setColumns(10);
		txtPais.setBounds(198, 345, 130, 20);
		panel.add(txtPais);
		
		final JButton Editar = new JButton("Editar");
	
		Editar.setFont(new Font("Arial", Font.BOLD, 11));
		Editar.setBounds(310, 455, 89, 23);
		panel.add(Editar);
		Editar.setVisible(false);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 118, 401, 377);
		panel.add(textPane);
		
		JLabel Usuarios = new JLabel("");
		Usuarios.setFont(new Font("Arial", Font.BOLD, 11));
		Usuarios.setBounds(65, 93, 57, 14);
		panel.add(Usuarios);
		
		JLabel lblNewLabel_8 = new JLabel("P\u00E1gina principal ");
		lblNewLabel_8.setForeground(new Color(0, 0, 0));
		lblNewLabel_8.setFont(new Font("Arial", Font.BOLD, 13));
		lblNewLabel_8.setBounds(58, 139, 124, 23);
		contentPane.add(lblNewLabel_8);
		
		JLabel lblForum = new JLabel("      Forum");
		lblForum.setForeground(new Color(0, 0, 0));
		lblForum.setFont(new Font("Arial", Font.BOLD, 13));
		lblForum.setBounds(301, 139, 111, 23);
		contentPane.add(lblForum);
		
		JLabel CerrarSesion = new JLabel("Cerrar sesi\u00F3n");
		CerrarSesion.setForeground(new Color(0, 0, 0));
		CerrarSesion.setFont(new Font("Arial", Font.BOLD, 13));
		CerrarSesion.setBounds(527, 139, 111, 23);
		contentPane.add(CerrarSesion);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.inactiveCaption);
		panel_1.setBounds(441, 179, 218, 506);
		contentPane.add(panel_1);
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setLayout(null);
		
		JLabel Restaurantes = new JLabel("            Restaurantes");
		Restaurantes.setFont(new Font("Arial", Font.BOLD, 12));
		Restaurantes.setBounds(42, 65, 123, 14);
		panel_1.add(Restaurantes);
		
		JLabel lblBares = new JLabel("                          Bares");
		lblBares.setFont(new Font("Arial", Font.BOLD, 12));
		lblBares.setBounds(23, 160, 142, 14);
		panel_1.add(lblBares);
		
		JLabel lblServicios = new JLabel("                       Servicios");
		lblServicios.setFont(new Font("Arial", Font.BOLD, 12));
		lblServicios.setBounds(23, 252, 173, 14);
		panel_1.add(lblServicios);
		
		JList list = new JList();
		list.setBackground(SystemColor.inactiveCaption);
		list.setBounds(23, 146, 173, -52);
		panel_1.add(list);
		
		JLabel lblNewLabel = new JLabel("Sus Comentarios");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNewLabel.setBounds(23, 11, 116, 27);
		panel_1.add(lblNewLabel);
		
		DefaultListModel listaRes= new DefaultListModel();
		JList ListaResta = new JList(listaRes);
		ListaResta.setBounds(10, 90, 198, 59);
		panel_1.add(ListaResta);
		
		DefaultListModel listaBares= new DefaultListModel();
		JList ListaBar = new JList(listaBares);
		ListaBar.setBounds(10, 185, 198, 59);
		panel_1.add(ListaBar);
		
		DefaultListModel listaServicios= new DefaultListModel();
		JList ListaServicio = new JList(listaServicios);
		ListaServicio.setBounds(10, 277, 198, 59);
		panel_1.add(ListaServicio);
		
		DefaultListModel listaHoteles= new DefaultListModel();
		listaHotel = new JList(listaHoteles);
		listaHotel.setBounds(10, 373, 198, 59);
		panel_1.add(listaHotel);
		
		JLabel lblRestaurantes = new JLabel("                  Hoteles");
		lblRestaurantes.setFont(new Font("Arial", Font.BOLD, 12));
		lblRestaurantes.setBounds(23, 347, 173, 14);
		panel_1.add(lblRestaurantes);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(664, 179, 17, 525);
		contentPane.add(scrollBar);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(EspacioPersonalApp.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(510, 24, 149, 91);
		contentPane.add(label_1);
		
		

// ........................................COdigo para obtener datos del usuario a inicair la sesion.........................
		

			 
		  	String Estado="si";

							 try {
								 String QueryPais="SELECT * FROM " + "USUARIOS"+" WHERE Usuario= '"+usuarios+"'"+"AND Clave='"+claves+"'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									 txtNombre.setText(ResultSet.getString("Nombre"));
									 txtApellido.setText(ResultSet.getString("Apellido"));
									 txtEmail.setText(ResultSet.getString("Email"));
									 txtCodigoPostal.setText(ResultSet.getString("Codigo_Postal"));
									 txtPviajes.setText(ResultSet.getString("Ultimo_Viaje"));
									 txtPais.setText(ResultSet.getString("Pais_Destino"));
									 txtOtrosDatos.setText(ResultSet.getString("Otros_Datos"));
									 Usuarios.setText(ResultSet.getString("Usuario"));
									 usuarios=ResultSet.getString("Usuario");

								 }
		
	} catch (Exception e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}	
	// ........................................FIN COdigo para obtener datos del usuario a inicair la sesion.........................
	
							 
// ........................................COdigo para obtener OPINIONES del usuario sobre el restaurante.........................
								
		 

						 try {
							 String QueryPais="SELECT * FROM " + "Opinar_restuarante";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st=Conexion.createStatement();
							 java.sql.ResultSet ResultSet;
							 ResultSet= st.executeQuery(QueryPais);
													 							 
							 
							 while (ResultSet.next())
							 {
								 //listaRes.addElement(ResultSet.getString("Nombre")+":");
								 listaRes.addElement(ResultSet.getString("Opinion"));
								 listaRes.addElement("\n");


								 
															
							 }
								
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
	// ........................................FIN COdigo para obtener la opinion.........................
														 
	// ........................................COdigo para obtener OPINIONES del usuario sobre el bar........................
							
						 

						 try {
							 String QueryPais="SELECT * FROM " + "Opinar_bar";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st=Conexion.createStatement();
							 java.sql.ResultSet ResultSet;
							 ResultSet= st.executeQuery(QueryPais);
													 							 
							 
							 while (ResultSet.next())
							 {
								 //listaRes.addElement(ResultSet.getString("Nombre")+":");
								 listaBares.addElement(ResultSet.getString("Opinion"));
								 listaBares.addElement("\n");


								 
															
							 }
								
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
	// ........................................FIN COdigo para obtener datos del opinion........................
														 							 
// ........................................COdigo para obtener OPINIONES del usuario sobre elservicos.......................
							
						 

						 try {
							 String QueryPais="SELECT * FROM " + "Opinar_servicios";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st=Conexion.createStatement();
							 java.sql.ResultSet ResultSet;
							 ResultSet= st.executeQuery(QueryPais);
													 							 
							 
							 while (ResultSet.next())
							 {
								 //listaRes.addElement(ResultSet.getString("Nombre")+":");
								 listaServicios.addElement(ResultSet.getString("Opinion"));
								 listaServicios.addElement("\n");


								 
															
							 }
								
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
	// ........................................FIN COdigo para obtener datos del opinion........................	
						 
						 
// ........................................COdigo para obtener OPINIONES del usuario sobre hoteles.......................
							
						 

						 try {
							 String QueryPais="SELECT * FROM " + "Opinar_hotel";
							 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
						     Statement st=Conexion.createStatement();
							 java.sql.ResultSet ResultSet;
							 ResultSet= st.executeQuery(QueryPais);
													 							 
							 
							 while (ResultSet.next())
							 {
								 //listaRes.addElement(ResultSet.getString("Nombre")+":");
								 listaServicios.addElement(ResultSet.getString("Opinion"));
								 listaServicios.addElement("\n");


								 
															
							 }
								
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}	
	// ........................................FIN COdigo para obtener datos del opinion........................
//------------------------------------pagina principal.................................................................
							 
		 MouseListener PaginaPrincipal=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   Pagina_Principal atras=new  Pagina_Principal();
			   atras.setVisible(true);				   }
		};
		lblNewLabel_8.addMouseListener(PaginaPrincipal);
		    	
		   
//------------------------------------fin ................................................................
		 
		
		
//------------------------------------------------- Evento para ir al forum...............................................
		
		 MouseListener Forum=new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   Forum_App PaginaForum=new  Forum_App(usuarios,claves);
				   PaginaForum.setVisible(true);				   }
			};
			lblForum.addMouseListener(Forum);
			    	
//-------------------------------------------------FIN  Evento para ir al forum...............................................
	
//..........................evento para ir al formulario de modificacion de datos..........................
			
			 MouseListener ModificarDatos=new MouseListener() {
				   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					 			
					   
					   txtNombre.setEditable(true);
					   txtApellido.setEditable(true);
					   txtEmail.setEditable(true);
					   txtCodigoPostal.setEditable(true);
					   txtPviajes.setEditable(true);
					   txtPais.setEditable(true);
					   txtOtrosDatos.setEditable(true);
					   Editar.setVisible(true);

					   
					 }
				};
				modificar.addMouseListener(ModificarDatos);
		
//........................................................evento para ir al formulario de cambio de contras.......................
		
			 MouseListener CambioCont=new MouseListener() {
				   
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
					   Cambiar_contrasena_Usuario_App cambiar=new   Cambiar_contrasena_Usuario_App(usuarios,claves);
					   cambiar.setVisible(true);				   
					 }
				};
				lblCambiarContrasea.addMouseListener(CambioCont);
				
				
				
//..................................... del codigo de guardar los cambios k ha hecho el usuario en la base de datos
				
				Editar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						
						SQL bd=new SQL();
						try {
							bd.SQLConnection("grupo1","root","");
							String nombre=txtNombre.getText();
				        	String apellido=txtApellido.getText();
				        	String email=txtEmail.getText();
				        	String codPostal0=txtCodigoPostal.getText();
				        	int codPostal= Integer.parseInt(codPostal0);
				        	String uViaje=txtPviajes.getText();
				        	String pDestino=txtPais.getText();
				        	String oDatos=txtOtrosDatos.getText();
							bd.UpdatedeleteRecord_Usuario(usuarios,nombre,apellido,email,codPostal,uViaje,pDestino,oDatos);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				});

//..................................... FIN del codigo de guardar los cambios k ha hecho el usuario en la base de datos.................

		
		
//..........................................evento para cerrar session......................................................
				 MouseListener CambioEstado=new MouseListener() {
					   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						  try {
					           
					        	String estado="no";
					        	String Query = "UPDATE USUARIOS SET Estado='"+estado+"'";
					        	
					            Statement st = Conexion.createStatement();
					            st.executeUpdate(Query);

					             Pagina_Principal principal=new Pagina_Principal();
					             principal.setVisible(true);
					             setVisible(false);
					            
					           // JOptionPane.showMessageDialog(null, "Sesi�n Cerrada");

					        } catch (SQLException ex) {
					            System.out.println(ex.getMessage());
					           // JOptionPane.showMessageDialog(null, "Error a cerrar sesi�n");
					        }  
						 
						 }
					};
					CerrarSesion.addMouseListener(CambioEstado);
					
	//..........................................FIN evento para cerrar session......................................................

	}
}
