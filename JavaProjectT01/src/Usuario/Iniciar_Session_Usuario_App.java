//Assiya Mrabet

package Usuario;

import java.awt.BorderLayout;
import WipAssiya.SQL;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.event.MouseListener;
import javax.swing.*;





public class Iniciar_Session_Usuario_App extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JPasswordField passwordField;
	public String usuarios="";
	public String claves="";


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Iniciar_Session_Usuario_App frame = new Iniciar_Session_Usuario_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Iniciar_Session_Usuario_App() {
		setTitle("Iniciar sesi\u00F3n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 598, 394);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Journey&Experience");
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setFont(new Font("Arial Black", Font.ITALIC, 30));
		lblNewLabel.setBounds(54, 36, 364, 43);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Iniciar_Session_Usuario_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		lblNewLabel_1.setBounds(430, 16, 141, 99);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Iniciar sesi\u00F3n");
		lblNewLabel_2.setFont(new Font("Arial Black", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(178, 125, 123, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Contrase\u00F1a");
		lblNewLabel_3.setFont(new Font("Arial", Font.PLAIN, 15));
		lblNewLabel_3.setBounds(126, 197, 97, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel label = new JLabel("Usuario\r\n\r\n");
		label.setFont(new Font("Arial", Font.PLAIN, 15));
		label.setBounds(126, 162, 97, 14);
		contentPane.add(label);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(233, 160, 143, 20);
		contentPane.add(textField_1);
		
		JLabel lblNewLabel_4 = new JLabel("\u00BFNo eres usuario? Registrate aqu\u00ED, es gratis.\r\n");
		lblNewLabel_4.setFont(new Font("Arial", Font.PLAIN, 13));
		lblNewLabel_4.setBounds(77, 296, 267, 37);
		contentPane.add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("Iniciar sesi\u00F3n");
		
		btnNewButton.setBounds(220, 262, 124, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Registrate\r\n");
		
		btnNewButton_1.setBounds(387, 304, 97, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNoMeAcuerdo = new JLabel("He olvidado mi contrase\u00F1a.");
		lblNoMeAcuerdo.setForeground(Color.BLUE);
		lblNoMeAcuerdo.setFont(new Font("Arial", Font.PLAIN, 13));
		lblNoMeAcuerdo.setBounds(199, 222, 156, 30);
		contentPane.add(lblNoMeAcuerdo);
		
		final JLabel lblNewLabel_5 = new JLabel("Usuario o contrase\u00F1a");
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_5.setForeground(Color.RED);
		lblNewLabel_5.setBounds(413, 173, 129, 14);
		contentPane.add(lblNewLabel_5);
		lblNewLabel_5.setVisible(false);
		
		final JLabel lblSonIncorrectos = new JLabel("son incorrectos");
		lblSonIncorrectos.setForeground(Color.RED);
		lblSonIncorrectos.setFont(new Font("Arial", Font.BOLD, 12));
		lblSonIncorrectos.setBounds(413, 198, 129, 14);
		contentPane.add(lblSonIncorrectos);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(233, 195, 143, 20);
		contentPane.add(passwordField);
		lblSonIncorrectos.setVisible(false);
		
		usuarios=textField_1.getText();
		claves=passwordField.getText().toString();
		//Button para iniciar session del usuario
		//Comprobar si contras. y usuario estan bien
//.................................................evento para iniciar sesion....................................		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SQL bd=new SQL();
				try {
					bd.SQLConnection("grupo1","root", "");
					if(bd.getValuesUsuario_Sesion(textField_1.getText(),passwordField.getText())) //comprobamos si el usuario esta registrado
					 {
						
						EspacioPersonalApp personal=new EspacioPersonalApp(textField_1.getText(),passwordField.getText());
						personal.setVisible(true);
						setVisible(false);

					 }
					else
					{
						lblSonIncorrectos.setVisible(true);
						lblNewLabel_5.setVisible(true);


					 }
						
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		
		
//...................................Abrir el formulario de registrarse..................................................
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false); //cerrar la ventana de iniciar sesion
				Registrar_Usuario_App registrar=new Registrar_Usuario_App();
				registrar.setVisible(true);
				
			}
		});
		//...................................Fin del codigo dAbrir el formulario de registrarse..................................................
	
//................................. evento a clicar sobre label,he olvidado la contras.......................................
		
		 MouseListener clikLabel=new MouseListener() {
				   
		  public void mouseReleased(MouseEvent arg0) { }	   
		  public void mousePressed(MouseEvent arg0) {  }		   
		  public void mouseExited(MouseEvent arg0) {  }
		  public void mouseEntered(MouseEvent arg0) { }
		  
		 public void mouseClicked(MouseEvent arg0) {
					  
			   Cambiar_contrasena_Usuario_App recuperar=new  Cambiar_contrasena_Usuario_App(usuarios,claves);
			   recuperar.setVisible(true);
			   setVisible(false);
			   }
		};
		lblNoMeAcuerdo.addMouseListener(clikLabel);
		
//............................................FIN dwl ocdigo de ir a la ventana he olvidado contra...............................
		
		/*blNoMeAcuerdo.addMouseListener(new MouseListener(){
		      public void mouseClicked(MouseEvent e) {
		        
		    	Recuperar_contrasenya_Usuario_App recuperar=new  Recuperar_contrasenya_Usuario_App();
		    	recuperar.setVisible(true);
		    	
		    	
		      }
		    });*/
		    
//.............................................................GET Y SET  USUARIO CONTRA.............................................................
	}		  


}	

