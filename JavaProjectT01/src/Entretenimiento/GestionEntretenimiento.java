package Entretenimiento;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Restaurantes.EliminarRestaurante_App;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GestionEntretenimiento extends JFrame {

	private JPanel contentPane;

	public  String claves="";
	public  String usuarios="";
	public String nombres="";
	public String nombreTable="";
	public int id_bar=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestionEntretenimiento frame = new GestionEntretenimiento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GestionEntretenimiento() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(Color.BLUE);
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 28, 303, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
	//	label_1.setIcon(new ImageIcon(GestionEntretenimiento.class.getResource("/Vistas/j-e.jpg")));
		label_1.setBounds(313, 11, 111, 76);
		contentPane.add(label_1);
		
		JButton button = new JButton("A\u00F1adir");
		
		button.setFont(new Font("Arial", Font.BOLD, 11));
		button.setBounds(77, 142, 89, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Editar");
	
		button_1.setFont(new Font("Arial", Font.BOLD, 11));
		button_1.setBounds(241, 142, 89, 23);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("Eliminar");
		
		button_2.setFont(new Font("Arial", Font.BOLD, 11));
		button_2.setBounds(77, 197, 89, 23);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("Mostrar");
		
		button_3.setFont(new Font("Arial", Font.BOLD, 11));
		button_3.setBounds(241, 197, 89, 23);
		contentPane.add(button_3);
		
		JLabel lblNewLabel = new JLabel("Gesti\u00F3n del entretenimiento");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel.setBounds(37, 97, 200, 14);
		contentPane.add(lblNewLabel);
		
		
//...................................llamar a la ventana de A�adir restaurante...................................................
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			A�adirEntre_App pais=new A�adirEntre_App();
			pais.setVisible(true);
			}
		});
// ...............................................fin llamada a laventana de a�adir.........................................//
		
//...................................llamar a la ventana de eliminar pais...................................................
		
		
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EliminarEntre_App eliminarPais=new EliminarEntre_App();
				eliminarPais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de A�adir pais...................................................
				
//...........................................llamar a la ventana a eliminar pais.....................................................
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditarEntre_App editarpais=new EditarEntre_App();
				editarpais.setVisible(true);
			}
		});

//...................................fin llamada a la ventana de eliminar pais...................................................
	
//....................................................llamar a la ventana para mostrar hoteles...........................//
		
		button_3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		 
			MostrarEntre_App mostrar=new MostrarEntre_App(usuarios,claves);
			mostrar.setVisible(true);
		
		}
		});
//...................................fin llamada a la ventana de mostrar pais...................................................

	}

}
