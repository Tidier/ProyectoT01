package Restaurantes;


import java.awt.BorderLayout;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import Pais.EliminarPaisApp;
import WipAssiya.SQL;
import Usuario.Comentarios_Usuario_APP;
import Usuario.EspacioPersonalApp;
import Usuario.Iniciar_Session_Usuario_App;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JTextArea;

public class MostrarRestauranteApp extends JFrame {

	private JPanel contentPane;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JTextField txtnombre;
	private JTextField txtpais;
	private JTextField txtciudad;
	private JTextField txtcontacto;
	private JTextField txtservicios;
	private JTextField texttarifa;
	int Id_Restaurante=0;
	public String usuarios="";
	public String claves="";
	public String nameTable="Restaurantes";
	public String nombre="";
	public String table="Opinar_restuarante";
	
	SQL bd=new SQL();
	private JTextField txtHorario;
	


	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MostrarHotel_App frame = new MostrarHotel_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MostrarRestauranteApp(String usuario,String clave) {
		
		this.usuarios=usuario;
		this.claves=clave;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 564, 597);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setBounds(27, 30, 335, 43);
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(360, 11, 148, 76);
		label_1.setIcon(new ImageIcon(MostrarRestauranteApp.class.getResource("/Vistas/iconoPrincipal.jpg")));
		contentPane.add(label_1);
;
		
		JLabel lblCiudadQueQuieres = new JLabel("Selecciona el restaurante");
		lblCiudadQueQuieres.setBounds(27, 154, 148, 14);
		lblCiudadQueQuieres.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblCiudadQueQuieres);
		
		final JComboBox ListaRestaurante = new JComboBox();
		ListaRestaurante.setBounds(231, 151, 140, 20);
		contentPane.add(ListaRestaurante);
	
//..........................codigo para obtener hoteles de BD y a�adirlos a combox...........................................//

		//SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesRestaurante();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaRestaurante.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(419, 150, 89, 23);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(btnNewButton);
		
		final JLabel lblDatosDelHotel = new JLabel("Datos del hotel");
		lblDatosDelHotel.setBounds(10, 190, 120, 14);
		lblDatosDelHotel.setFont(new Font("Arial", Font.BOLD, 12));
		contentPane.add(lblDatosDelHotel);
		lblDatosDelHotel.setVisible(false);
		
		final JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(106, 215, 79, 14);
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblNombre);
		lblNombre.setVisible(false);
		
		final JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(106, 255, 68, 14);
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblPais);
		lblPais.setVisible(false);

		
		final JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setBounds(106, 280, 68, 14);
		lblCiudad.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblCiudad);
		lblCiudad.setVisible(false);
		

		
		final JLabel lblContacto = new JLabel("Contacto");
		lblContacto.setBounds(106, 317, 68, 14);
		lblContacto.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblContacto);
		lblContacto.setVisible(false);
		
		final JLabel lblServicios = new JLabel("Especialidad");
		lblServicios.setBounds(106, 342, 89, 14);
		lblServicios.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblServicios);
		lblServicios.setVisible(false);
	
		
		final JLabel lblTarifas = new JLabel("Tarifas");
		lblTarifas.setBounds(106, 373, 68, 14);
		lblTarifas.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblTarifas);
		lblTarifas.setVisible(false);

		
		final JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setBounds(106, 437, 89, 14);
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblDescripcin);
		lblDescripcin.setVisible(false);

		
		txtnombre = new JTextField();
		txtnombre.setBounds(212, 215, 179, 20);
		txtnombre.setEditable(false);
		contentPane.add(txtnombre);
		txtnombre.setColumns(10);
		txtnombre.setVisible(false);
	
		
		txtpais = new JTextField();
		txtpais.setBounds(212, 246, 179, 20);
		txtpais.setEditable(false);
		txtpais.setColumns(10);
		contentPane.add(txtpais);
		txtpais.setVisible(false);
	
		
		txtciudad = new JTextField();
		txtciudad.setBounds(212, 277, 179, 20);
		txtciudad.setEditable(false);
		txtciudad.setColumns(10);
		contentPane.add(txtciudad);
		txtciudad.setVisible(false);

		
		txtcontacto = new JTextField();
		txtcontacto.setBounds(212, 308, 179, 20);
		txtcontacto.setEditable(false);
		txtcontacto.setColumns(10);
		contentPane.add(txtcontacto);
		txtcontacto.setVisible(false);

		
		txtservicios = new JTextField();
		txtservicios.setBounds(212, 339, 179, 20);
		txtservicios.setEditable(false);
		txtservicios.setColumns(10);
		contentPane.add(txtservicios);
		txtservicios.setVisible(false);

		
		
		texttarifa = new JTextField();
		texttarifa.setBounds(212, 370, 179, 20);
		texttarifa.setEditable(false);
		texttarifa.setColumns(10);
		contentPane.add(texttarifa);
		texttarifa.setVisible(false);

		
		final JTextArea txtdescripcion = new JTextArea();
		txtdescripcion.setBounds(212, 432, 240, 69);
		txtdescripcion.setEditable(false);
		contentPane.add(txtdescripcion);
		
		final JButton lblComentar = new JButton("Comentar");
		lblComentar.setBounds(449, 512, 89, 23);
		
		lblComentar.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblComentar);
		lblComentar.setVisible(false);
		
		final JLabel lblHorario = new JLabel("Horario");
		lblHorario.setBounds(106, 404, 68, 14);
		lblHorario.setFont(new Font("Arial", Font.BOLD, 11));
		contentPane.add(lblHorario);
		lblHorario.setVisible(false);
		
		
		txtHorario = new JTextField();
		txtHorario.setBounds(212, 401, 179, 20);
		txtHorario.setEditable(false);
		contentPane.add(txtHorario);
		txtHorario.setColumns(10);
		
		JLabel principal = new JLabel("P\u00E1gina principal ");
		principal.setForeground(Color.BLACK);
		principal.setFont(new Font("Arial", Font.BOLD, 13));
		principal.setBounds(27, 100, 124, 23);
		contentPane.add(principal);
		
		JLabel lblForum = new JLabel("      Forum");
		lblForum.setForeground(Color.BLACK);
		lblForum.setFont(new Font("Arial", Font.BOLD, 13));
		lblForum.setBounds(225, 100, 111, 23);
		contentPane.add(lblForum);
		
		JLabel iniciar = new JLabel("Iniciar sesion");
		iniciar.setForeground(Color.BLACK);
		iniciar.setFont(new Font("Arial", Font.BOLD, 13));
		iniciar.setBounds(412, 100, 111, 23);
		contentPane.add(iniciar);
		txtHorario.setVisible(false);
		txtdescripcion.setVisible(false);


	
	
		

				 
				 
//.................................... evento para mostrar datos de la ciudad selecionada......................................	
					
				 btnNewButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
								
							//SQL bd=new SQL();
						      //int Contacto=Integer.parseInt(txtContacto.getText()); // pasamos contacto a int

							 
							 try {
								 String Restaurante= ListaRestaurante.getSelectedItem().toString();

								 String QueryPais="SELECT * FROM " + "RESTAURANTES"+ " WHERE NOMBRE= '"+Restaurante+"'";
								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryPais);
								 
								 while (ResultSet.next())
								 {
									txtnombre.setText(ResultSet.getString("Nombre"));
									txtcontacto.setText(ResultSet.getString("Contacto"));
									txtservicios.setText(ResultSet.getString("Especialidad"));
									txtdescripcion.setText(ResultSet.getString("Descripcion"));
									texttarifa.setText(ResultSet.getString("Tarifas"));
									txtHorario.setText(ResultSet.getString("Horario"));
									Id_Restaurante=ResultSet.getInt("Id_Restaurante");
									nombre=(ResultSet.getString("Nombre"));


								 }
								 
							 }catch (Exception e2) {
										// TODO Auto-generated catch block
										e2.printStackTrace();
									}
								 

								 try {
									 String hotel= ListaRestaurante.getSelectedItem().toString();
										
										bd.SQLConnection("grupo1","root","");


									 String QueryPais="SELECT * FROM " + "tener_restaurante"+ " WHERE Id_restaurante= "+Id_Restaurante;
									 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
									 Statement st=Conexion.createStatement();
									 java.sql.ResultSet ResultSet;
									 ResultSet= st.executeQuery(QueryPais);
									 
									 while (ResultSet.next())
									 {
										String  nom_ciudad=bd.getValues_NombreCiudad(ResultSet.getInt("id_ciudad"));
										 txtciudad.setText(nom_ciudad);
										txtpais.setText(ResultSet.getString("Nombre_Pais"));


									 }
							// hacer visible los campos para que el admin. modifique los datos de la ciudad selecionada
									txtdescripcion.setVisible(true);
									texttarifa.setVisible(true);
									txtservicios.setVisible(true);
									txtcontacto.setVisible(true);
									txtciudad.setVisible(true);
									txtpais.setVisible(true);
									txtnombre.setVisible(true);
									txtHorario.setVisible(true);
									lblDescripcin.setVisible(true);
									lblTarifas.setVisible(true);
									lblServicios.setVisible(true);
									lblCiudad.setVisible(true);
									lblPais.setVisible(true);
									lblDatosDelHotel.setVisible(true);
									lblNombre.setVisible(true);
									lblContacto.setVisible(true);
									lblHorario.setVisible(true);
									lblComentar.setVisible(true);




						
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
							}
							
						

						}
					});
					bd.closeConnection();
//....................................fin  evento para mostrar datos de la ciudad selecionada......................................	

				
					
	//..................................... evento a clicar sobre label,atras............................................
					
					 MouseListener clikLabel=new MouseListener() {
							   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   GestionRestaurante atras=new GestionRestaurante();
						   atras.setVisible(true);				   }
					};
					 				
//..................................... FIN evento a clicar sobre label,atras............................................

					
//..................................... evento para ir a la pantalla de comentar............................................

					 lblComentar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
								Comentarios_Usuario_APP comenta=new Comentarios_Usuario_APP(usuarios,claves,nombre,nameTable,Id_Restaurante,table);
								comenta.setVisible(true);
								setVisible(false);
							}
						});

	//..................................... FIN evento para ir a la pantalla de comentar............................................
					
					 
					 //------------------------------------------------- Evento para ir al forum...............................................
						
					 MouseListener Forum=new MouseListener() {
						   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Forum_App PaginaForum=new  Forum_App(usuarios,claves);
							   PaginaForum.setVisible(true);				   }
						};
						lblForum.addMouseListener(Forum);
						    	
			//-------------------------------------------------FIN  Evento para ir al forum...............................................
	
						
						//------------------------------------pagina principal.................................................................
						 
						 MouseListener PaginaPrincipal=new MouseListener() {
								   
						  public void mouseReleased(MouseEvent arg0) { }	   
						  public void mousePressed(MouseEvent arg0) {  }		   
						  public void mouseExited(MouseEvent arg0) {  }
						  public void mouseEntered(MouseEvent arg0) { }
						  
						 public void mouseClicked(MouseEvent arg0) {
									  
							   Pagina_Principal atras=new  Pagina_Principal();
							   atras.setVisible(true);				   }
						};
						principal.addMouseListener(PaginaPrincipal);
						    	
						   
				//------------------------------------fin ................................................................
						 
						//LISTENER DE LABEL INICIAR SESION
						MouseListener iniSesion =new MouseListener() {
							   
							  public void mouseReleased(MouseEvent arg0) { }	   
							  public void mousePressed(MouseEvent arg0) {  }		   
							  public void mouseExited(MouseEvent arg0) {  }
							  public void mouseEntered(MouseEvent arg0) { }
							  
							 public void mouseClicked(MouseEvent arg0) {
										  
								   Iniciar_Session_Usuario_App inicio = new Iniciar_Session_Usuario_App();
								   inicio.setVisible(true);
								   
								   }
							};
						iniciar.addMouseListener(iniSesion);
					 
					 
	}
}

