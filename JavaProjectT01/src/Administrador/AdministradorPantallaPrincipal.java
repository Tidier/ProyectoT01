package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import Forum.Forum_App;
import Main_Comun_App.Pagina_Principal;
import Main_Comun_App.SQL;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.LineBorder;

public class AdministradorPantallaPrincipal extends JFrame {

	private JPanel contentPane;
    public JFrame frame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministradorPantallaPrincipal frame = new AdministradorPantallaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdministradorPantallaPrincipal() {
		setResizable(false);
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.controlLtHighlight);
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(140, 87, 238, 330);
		panel.add(panel_3);
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel_3.setLayout(null);
		
		JLabel imagenAdmin = new JLabel("");
		imagenAdmin.setBounds(24, 36, 189, 212);
		panel_3.add(imagenAdmin);
		imagenAdmin.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		imagenAdmin.setVerticalAlignment(SwingConstants.TOP);
		imagenAdmin.setHorizontalAlignment(SwingConstants.CENTER);
		imagenAdmin.setIcon(new ImageIcon(AdministradorPantallaPrincipal.class.getResource("/Vistas/admin.jpg")));
		
		final JLabel lblCuenta = DefaultComponentFactory.getInstance().createTitle("Cuenta");
		lblCuenta.setBounds(94, 276, 88, 14);
		panel_3.add(lblCuenta);
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 16));
		
		JLabel lblAdmin = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdmin.setBounds(91, 259, 88, 14);
		panel_3.add(lblAdmin);
		lblAdmin.setForeground(new Color(60, 179, 113));
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 20));

		
		
		//TEXTO "JOURNEY & EXPERIENCE"
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setBounds(561, 87, 222, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 18));
		
		JLabel label_1 = new JLabel("");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setIcon(new ImageIcon(AdministradorPantallaPrincipal.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(578, 11, 143, 91);
		panel.add(label_1);
		
		//IMAGEN FONDO
		
		JLabel ImagenFondo = new JLabel("");
		ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
		ImagenFondo.setBounds(0, 0, 525, 562);
		panel.add(ImagenFondo);
		
		
		//TEXTO "PANEL DE CONTROL"
		JLabel lblPanelDeControl = DefaultComponentFactory.getInstance().createTitle("Panel de control");
		lblPanelDeControl.setBackground(new Color(127, 255, 0));
		lblPanelDeControl.setBounds(578, 176, 171, 14);
		panel.add(lblPanelDeControl);
		lblPanelDeControl.setForeground(new Color(0, 255, 0));
		lblPanelDeControl.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 20));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel_2.setBackground(new Color(119, 136, 153));
		panel_2.setForeground(new Color(119, 136, 153));
		panel_2.setBounds(543, 159, 224, 382);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		
		//BOTON GESTIONAR
		
		JButton btnGestionar = new JButton("Gestionar \r\nPantallas");
		btnGestionar.setBounds(38, 53, 149, 44);
		panel_2.add(btnGestionar);
		
		
		//BOTON USUARIOS
		JButton btnUsuarios = new JButton("Usuarios");
		btnUsuarios.setBounds(38, 120, 149, 44);
		panel_2.add(btnUsuarios);
		
		//BOTON SUPER USUARIOS
		
		JButton btnSuperusuarios = new JButton("SuperUsuarios");
		btnSuperusuarios.setBounds(38, 175, 149, 44);
		panel_2.add(btnSuperusuarios);
		
		//BOTON ADMINISTRADORES
		
		JButton btnAdministradores = new JButton("Administradores");
		btnAdministradores.setForeground(new Color(0, 0, 0));
		btnAdministradores.setBackground(UIManager.getColor("Button.background"));
		btnAdministradores.setBounds(38, 230, 149, 50);
		panel_2.add(btnAdministradores);
		
		//BotonInicio
		JLabel btnInicio = new JLabel("");
		btnInicio.setIcon(new ImageIcon(AdministradorPantallaPrincipal.class.getResource("/Vistas/inicio00.png")));
		btnInicio.setBounds(87, 308, 59, 63);
		panel_2.add(btnInicio);
			
		
		//LISTENERS
		
		
		//LISTENER DEL BOTON GESTIONAR
		btnGestionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministradorControlaTodo ver = new AdministradorControlaTodo();
		        ver.setVisible(true);
		        dispose();
			}
		});
		
		//LISTENER DEL BOTON USUARIOS
		btnUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministradorListaUsuarios ver = new AdministradorListaUsuarios();
		        ver.setVisible(true);
		        dispose();
			}
		});
		
		
		//LISTENER DEL BOTON SUPERUSUARIOS
		btnSuperusuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministradorListaSuperUsuarios ver = new AdministradorListaSuperUsuarios();
		        ver.setVisible(true);
		        dispose();
			}
		});
		
		//LISTENER DEL BOTON ADMINISTRADORES
		btnAdministradores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministradorListaAdministradores ver = new AdministradorListaAdministradores();
				ver.setVisible(true);
				dispose();
			}
		});
		
		
		//LISTENER DE LABEL CUENTA
		MouseListener cuenta =new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   AdministradorCuenta cuenta=new AdministradorCuenta();
				   cuenta.setVisible(true);
				   
				   dispose();
				   }
			};
			lblCuenta.addMouseListener(cuenta);
			
		//LISTENER DE LABEL INICIO
		MouseListener inicio =new MouseListener() {
				   
			 public void mouseReleased(MouseEvent arg0) { }	   
			 public void mousePressed(MouseEvent arg0) {  }		   
			 public void mouseExited(MouseEvent arg0) {  }
			 public void mouseEntered(MouseEvent arg0) { }
				  
			public void mouseClicked(MouseEvent arg0) {
							  
				Pagina_Principal p = new Pagina_Principal();
				p.setVisible(true);
				dispose();
			  }
			};
		    btnInicio.addMouseListener(inicio);			
			

		

	}
}
