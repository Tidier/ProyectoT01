package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import Main_Comun_App.SQL;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import javax.swing.JTextField;

public class AdministradorCuenta extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellidos;
	private JTextField txtdni;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministradorCuenta frame = new AdministradorCuenta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdministradorCuenta() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.activeCaption);
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(UIManager.getBorder("RadioButton.border"));
		panel_1.setBounds(151, 76, 508, 392);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAdministrarUsuarios = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdministrarUsuarios.setForeground(Color.BLUE);
		lblAdministrarUsuarios.setBounds(207, 151, 76, 22);
		panel_1.add(lblAdministrarUsuarios);
		lblAdministrarUsuarios.setFont(new Font("Arial", Font.BOLD, 18));
		
		JLabel lblListaDeUsuarios = DefaultComponentFactory.getInstance().createTitle("Informaci\u00F3n:");
		lblListaDeUsuarios.setFont(new Font("Arial", Font.BOLD, 17));
		lblListaDeUsuarios.setBounds(20, 181, 149, 14);
		panel_1.add(lblListaDeUsuarios);
		
		JLabel lblUsuario_1 = DefaultComponentFactory.getInstance().createLabel("Nombre");
		lblUsuario_1.setFont(new Font("Arial", Font.BOLD, 12));
		lblUsuario_1.setBounds(20, 208, 88, 14);
		panel_1.add(lblUsuario_1);
		
		JLabel lblUsuario_2 = DefaultComponentFactory.getInstance().createLabel("Apellidos");
		lblUsuario_2.setFont(new Font("Arial", Font.BOLD, 12));
		lblUsuario_2.setBounds(20, 233, 88, 14);
		panel_1.add(lblUsuario_2);
		
		
		//RADIO BUTTONS------------------------------------------------------
		final JRadioButton oAdmin = new JRadioButton("Admin");
		oAdmin.setSelected(true);
		oAdmin.setEnabled(false);
		oAdmin.setFont(new Font("Arial", Font.PLAIN, 11));
		oAdmin.setBounds(395, 244, 61, 23);
		panel_1.add(oAdmin);
		
		final JRadioButton oSuperUser = new JRadioButton("SuperUser");
		oSuperUser.setEnabled(false);
		oSuperUser.setFont(new Font("Arial", Font.PLAIN, 11));
		oSuperUser.setBounds(395, 270, 80, 23);
		panel_1.add(oSuperUser);
		
		final JRadioButton oEstandar = new JRadioButton("Estandar");
		oEstandar.setEnabled(false);
		oEstandar.setFont(new Font("Arial", Font.PLAIN, 11));
		oEstandar.setBounds(395, 296, 80, 23);
		panel_1.add(oEstandar);
		
		//Agrupamos los botones para que no sean independientes
		ButtonGroup opPermisos=new ButtonGroup();
		opPermisos.add(oAdmin);
        opPermisos.add(oSuperUser);
        opPermisos.add(oEstandar);
		//-------------------------------------------------------------------
		JButton btnEditar = new JButton("Editar");
		btnEditar.setBounds(323, 358, 80, 23);
		panel_1.add(btnEditar);
		
		JButton btnBorrar_1 = new JButton("Borrar");
		btnBorrar_1.setBounds(413, 358, 76, 23);
		panel_1.add(btnBorrar_1);
		
		JLabel lblRol = DefaultComponentFactory.getInstance().createTitle("Rol Actual");
		lblRol.setFont(new Font("Arial", Font.BOLD, 12));
		lblRol.setBounds(323, 248, 88, 14);
		panel_1.add(lblRol);
		
		JLabel cambiarContraseņa = DefaultComponentFactory.getInstance().createTitle("Cambiar Contrase\u00F1a");
		cambiarContraseņa.setFont(new Font("Arial", Font.BOLD, 12));
		cambiarContraseņa.setBounds(20, 301, 139, 14);
		panel_1.add(cambiarContraseņa);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(86, 206, 86, 20);
		panel_1.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellidos = new JTextField();
		txtApellidos.setEditable(false);
		txtApellidos.setBounds(86, 231, 86, 20);
		panel_1.add(txtApellidos);
		txtApellidos.setColumns(10);
		
		txtdni = new JTextField();
		txtdni.setEditable(false);
		txtdni.setBounds(45, 258, 86, 20);
		panel_1.add(txtdni);
		txtdni.setColumns(10);
		
		//BOTON GUARDAR
		final JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(10, 336, 89, 23);
		panel_1.add(btnGuardar);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setForeground(Color.WHITE);
		lblNewJgoodiesTitle.setBounds(532, 11, 222, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 20));
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setFont(new Font("Arial", Font.BOLD, 12));
		lblDni.setBounds(20, 258, 46, 14);
		panel_1.add(lblDni);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setBounds(710, 92, 51, 51);
		panel.add(btnAtras);
		
		SQL db=new SQL();
		//LLAMAMOS LOS VALORES DE LA BASE DE DATOS A LOS TEXTFIELD----------------------------------
		try {
			db.SQLConnection("grupo1","root","");
		    
			txtNombre.setText(db.getValuesAdministrador("Admin")[3]);
			txtApellidos.setText(db.getValuesAdministrador("Admin")[4]);
			txtdni.setText(db.getValuesAdministrador("Admin")[0]);
			
			JLabel imagenAd = new JLabel("");
			imagenAd.setVerticalAlignment(SwingConstants.TOP);
			imagenAd.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/admin.jpg")));
			imagenAd.setBounds(110, 11, 190, 137);
			panel_1.add(imagenAd);
			
			//IMAGEN FONDO
			
			JLabel ImagenFondo = new JLabel("");
			ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
			ImagenFondo.setBounds(0, 0, 784, 562);
			panel.add(ImagenFondo);
			
		
			db.closeConnection();
			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}		
		//-------------------------------------------------------------------------------------------		
		
		//LISTENER BOTON GUARDAR
		btnGuardar.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
            SQL db = new SQL();
            
            String dni=txtdni.getText();
        	String nombre=txtNombre.getText();
        	String apellido=txtApellidos.getText();
        	
        	
            try {
            	db.SQLConnection("grupo1","root","");
            	db.modValuesAdministrador(dni,nombre,apellido,"Admin");
				txtNombre.setEditable(false);
				txtApellidos.setEditable(false);
				txtdni.setEditable(false);
				oAdmin.setEnabled(false);
				oSuperUser.setEnabled(false);
				oEstandar.setEnabled(false);
				btnGuardar.setEnabled(false);
            	//db.closeConnection();
            	
            	
            }catch (Exception e2) {
    			// TODO Auto-generated catch block
    			e2.printStackTrace();
    		}	
            	
            //System.out.println(txtNombre.getText());
		}
	});
		//LISTENER EDITAR
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNombre.setEditable(true);
				txtApellidos.setEditable(true);
				txtdni.setEditable(true);
				oAdmin.setEnabled(true);
				oSuperUser.setEnabled(true);
				oEstandar.setEnabled(true);
				btnGuardar.setEnabled(true);
				
			}
		});
		
		//LISTENER DE LABEL CAMBIAR CONTRASEŅA
		MouseListener contraseņa =new MouseListener() {
			  
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   AdministradorCambiarContraseņa contraseņa=new AdministradorCambiarContraseņa();
				   contraseņa.setVisible(true);
				   
				   }
			};
			cambiarContraseņa.addMouseListener(contraseņa);
			
			
			//LISTENER DE LABEL ATRAS
			MouseListener patras =new MouseListener() {
				  
				  public void mouseReleased(MouseEvent arg0) { }	   
				  public void mousePressed(MouseEvent arg0) {  }		   
				  public void mouseExited(MouseEvent arg0) {  }
				  public void mouseEntered(MouseEvent arg0) { }
				  
				 public void mouseClicked(MouseEvent arg0) {
							  
						AdministradorPantallaPrincipal adminlist= new AdministradorPantallaPrincipal();
						adminlist.setVisible(true);
						dispose();
					   
					   }
				};
				btnAtras.addMouseListener(patras);
		

	}
}
