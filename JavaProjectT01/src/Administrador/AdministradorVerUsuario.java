package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;
import java.util.logging.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.sun.jmx.snmp.daemon.CommunicationException;

import Main_Comun_App.SQL;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.SystemColor;
import javax.swing.JTextField;

public class AdministradorVerUsuario extends JFrame {
	
	 
	public JPanel contentPane;
	public JTextField txtNombre;
	public JTextField txtApellidos;
	public JTextField txtCodigoPostal;
	public JTextField txtEmail;
	public JTextField txtUltimoViaje;
	public JTextField txtPdestino;
	public JTextField txtOtrosDatos;
	public JLabel usuario;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					AdministradorVerUsuario frame = new AdministradorVerUsuario();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public AdministradorVerUsuario() {
		setResizable(false);
		
		
        
        
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 250, 250));
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCuenta = DefaultComponentFactory.getInstance().createTitle("Cuenta");
		lblCuenta.setForeground(Color.WHITE);
		lblCuenta.setBounds(141, 37, 88, 14);
		panel.add(lblCuenta);
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 12));
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(18, 11, 113, 132);
		panel.add(label_1);
		label_1.setVerticalAlignment(SwingConstants.TOP);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		JLabel lblAdmin = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 16));
		lblAdmin.setBounds(141, 21, 88, 14);
		panel.add(lblAdmin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(UIManager.getBorder("RadioButton.border"));
		panel_1.setBounds(151, 76, 508, 392);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		usuario = DefaultComponentFactory.getInstance().createTitle("");
		usuario.setForeground(new Color(154, 205, 50));
		usuario.setBounds(10, 8, 96, 22);
		usuario.setFont(new Font("Arial", Font.BOLD, 18));
		panel_1.add(usuario);
		
		JLabel informacion = DefaultComponentFactory.getInstance().createTitle("Informaci\u00F3n:");
		informacion.setFont(new Font("Arial", Font.BOLD, 17));
		informacion.setBounds(10, 41, 149, 14);
		panel_1.add(informacion);
		
		JLabel nombre = DefaultComponentFactory.getInstance().createLabel("Nombre:");
		nombre.setFont(new Font("Arial", Font.BOLD, 12));
		nombre.setBounds(20, 79, 88, 14);
		panel_1.add(nombre);
		
		JLabel apellidos = DefaultComponentFactory.getInstance().createLabel("Apellidos:");
		apellidos.setFont(new Font("Arial", Font.BOLD, 12));
		apellidos.setBounds(20, 104, 88, 14);
		panel_1.add(apellidos);
		
		//RADIO BUTTONS-------------------------------------------------------------------------------------------------
		final JRadioButton oAdmin = new JRadioButton("Admin");
		oAdmin.setEnabled(false);
		oAdmin.setFont(new Font("Arial", Font.PLAIN, 11));
		oAdmin.setBounds(391, 207, 61, 23);
		panel_1.add(oAdmin);
		
		final JRadioButton oSuperUser = new JRadioButton("SuperUser");
		oSuperUser.setEnabled(false);
		oSuperUser.setFont(new Font("Arial", Font.PLAIN, 11));
		oSuperUser.setBounds(391, 233, 80, 23);
		panel_1.add(oSuperUser);
		
		final JRadioButton oEstandar = new JRadioButton("Estandar");
		oEstandar.setEnabled(false);
		oEstandar.setSelected(true);
		oEstandar.setFont(new Font("Arial", Font.PLAIN, 11));
		oEstandar.setBounds(391, 259, 69, 23);
		panel_1.add(oEstandar);
		
		//Agrupamos los botones para que no sean independientes
		ButtonGroup opPermisos=new ButtonGroup();
		opPermisos.add(oAdmin);
        opPermisos.add(oSuperUser);
        opPermisos.add(oEstandar);		
		
	    //---------------------------------------------------------------------------------------------------------------
		
		JButton btnEditar= new JButton("Editar");
		btnEditar.setBounds(319, 358, 75, 23);
		panel_1.add(btnEditar);
		
		JButton btnBorrar_1 = new JButton("Borrar");
		btnBorrar_1.setBounds(404, 358, 75, 23);
		panel_1.add(btnBorrar_1);
		
		JLabel codPostal = DefaultComponentFactory.getInstance().createTitle("Codigo Postal:");
		codPostal.setFont(new Font("Arial", Font.BOLD, 12));
		codPostal.setBounds(20, 129, 88, 14);
		panel_1.add(codPostal);
		
		JLabel email = DefaultComponentFactory.getInstance().createTitle("Email:");
		email.setFont(new Font("Arial", Font.BOLD, 12));
		email.setBounds(20, 154, 88, 14);
		panel_1.add(email);
		
		JLabel uViaje = DefaultComponentFactory.getInstance().createTitle("Ultimo Viaje:");
		uViaje.setFont(new Font("Arial", Font.BOLD, 12));
		uViaje.setBounds(20, 182, 88, 14);
		panel_1.add(uViaje);
		
		JLabel rolActual = DefaultComponentFactory.getInstance().createTitle("Rol Actual");
		rolActual.setFont(new Font("Arial", Font.BOLD, 12));
		rolActual.setBounds(319, 211, 88, 14);
		panel_1.add(rolActual);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/avatar.jpg")));
		label_2.setBounds(340, 8, 149, 182);
		panel_1.add(label_2);
		
		JLabel destinoP = new JLabel("Destino preferido:");
		destinoP.setFont(new Font("Arial", Font.BOLD, 12));
		destinoP.setBounds(20, 210, 106, 14);
		panel_1.add(destinoP);
		
		JLabel otrosDatos = new JLabel("Otros Datos:");
		otrosDatos.setFont(new Font("Arial", Font.BOLD, 12));
		otrosDatos.setBounds(20, 237, 80, 14);
		panel_1.add(otrosDatos);
		
		//CAMPO TEXTO NOMBRE
		txtNombre = new JTextField();
		txtNombre.setToolTipText("");
		txtNombre.setEditable(false);
		txtNombre.setBounds(86, 77, 149, 20);
		panel_1.add(txtNombre);
		
		//CAMPO TEXTO APELLIDOS
		txtApellidos = new JTextField();
		txtApellidos.setBounds(86, 102, 149, 20);
		panel_1.add(txtApellidos);
		txtApellidos.setColumns(10);
		txtApellidos.setEditable(false);
		
		//CAMPO TEXTO CODIGO POSTAL
		txtCodigoPostal = new JTextField();
		txtCodigoPostal.setBounds(110, 127, 86, 20);
		panel_1.add(txtCodigoPostal);
		txtCodigoPostal.setColumns(10);
		txtCodigoPostal.setEditable(false);
		
		//CAMPO TEXTO E-MAIL
		txtEmail = new JTextField();
		txtEmail.setBounds(66, 154, 169, 20);
		panel_1.add(txtEmail);
		txtEmail.setColumns(10);
		txtEmail.setEditable(false);
		
		//CAMPO TEXTO ULTIMO VIAJE
		txtUltimoViaje = new JTextField();
		txtUltimoViaje.setBounds(127, 180, 108, 20);
		panel_1.add(txtUltimoViaje);
		txtUltimoViaje.setColumns(10);
		txtUltimoViaje.setEditable(false);
		
		//CAMPO TEXTO PAIS DESTINO
		txtPdestino= new JTextField();
		txtPdestino.setEditable(false);
		txtPdestino.setBounds(137, 208, 98, 20);
		panel_1.add(txtPdestino);
		txtPdestino.setColumns(10);
		
		//CAMPO TEXTO OTROS DATOS
		txtOtrosDatos = new JTextField();
		txtOtrosDatos.setEditable(false);
		txtOtrosDatos.setBounds(110, 234, 124, 20);
		panel_1.add(txtOtrosDatos);
		txtOtrosDatos.setColumns(10);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setForeground(Color.WHITE);
		lblNewJgoodiesTitle.setBackground(Color.WHITE);
		lblNewJgoodiesTitle.setBounds(493, 11, 231, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 20));
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setBounds(710, 92, 51, 51);
		panel.add(btnAtras);
		
		final JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(50, 358, 88, 23);
		btnGuardar.setEnabled(false);
		panel_1.add(btnGuardar);
		
		//IMAGEN FONDO
		
		JLabel ImagenFondo = new JLabel("");
		ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
		ImagenFondo.setBounds(0, 0, 784, 562);
		panel.add(ImagenFondo);
		
		SQL db=new SQL();
		//LLAMAMOS LOS VALORES DE LA BASE DE DATOS A LOS TEXTFIELD----------------------------------
		try {
			db.SQLConnection("grupo1","root","");
		    
			usuario.setText(db.getValuesUsuario("arkantoxs")[0]);
			
			txtNombre.setText(db.getValuesUsuario("arkantoxs")[2]);
			txtApellidos.setText(db.getValuesUsuario("arkantoxs")[3]);
			txtEmail.setText(db.getValuesUsuario("arkantoxs")[4]);
			txtCodigoPostal.setText(db.getValuesUsuario("arkantoxs")[5]);
			txtUltimoViaje.setText(db.getValuesUsuario("arkantoxs")[6]);	
			txtPdestino.setText(db.getValuesUsuario("arkantoxs")[7]);
			txtOtrosDatos.setText(db.getValuesUsuario("arkantoxs")[9]);
			

		
			db.closeConnection();
			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}		
		
		
		
		//-------------------------------------------------------------------------------------------
		/*//LISTENER BOTON SET
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNombre.setText(t);
				
			}
		});	*/
		


		
		//LISTENER DE LABEL ATRAS
		MouseListener patras =new MouseListener() {
			  
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
					AdministradorListaUsuarios lusuarios= new AdministradorListaUsuarios();
					lusuarios.setVisible(true);
					dispose();
				   
				   }
			};
			btnAtras.addMouseListener(patras);			
		
		
		
		//LISTENER EDITAR
			btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				txtNombre.setEditable(true);
				txtApellidos.setEditable(true);
				txtCodigoPostal.setEditable(true);
				txtEmail.setEditable(true);
				txtUltimoViaje.setEditable(true);
				txtPdestino.setEditable(true);
				txtOtrosDatos.setEditable(true);
				oAdmin.setEnabled(true);
				oSuperUser.setEnabled(true);
				oEstandar.setEnabled(true);
				btnGuardar.setEnabled(true);
			}
		});
			
		//LISTENER BOTON GUARDAR
			btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                SQL db = new SQL();
                
	        	String nombre=txtNombre.getText();
	        	String apellido=txtApellidos.getText();
	        	String email=txtEmail.getText();
	        	String codPostal0=txtCodigoPostal.getText();
	        	int codPostal= Integer.parseInt(codPostal0);
	        	String uViaje=txtUltimoViaje.getText();
	        	String pDestino=txtPdestino.getText();
	        	String oDatos=txtOtrosDatos.getText();
	        	
                try {
                	db.SQLConnection("grupo1","root","");
                	db.modValuesUsuario(nombre,apellido,email,codPostal,uViaje,pDestino,oDatos,"arkantoxs");
                	//db.closeConnection();
                	txtNombre.setEditable(false);
    				txtApellidos.setEditable(false);
    				txtCodigoPostal.setEditable(false);
    				txtEmail.setEditable(false);
    				txtUltimoViaje.setEditable(false);
    				txtPdestino.setEditable(false);
    				txtOtrosDatos.setEditable(false);
    				oAdmin.setEnabled(false);
    				oSuperUser.setEnabled(false);
    				oEstandar.setEnabled(false);
    				btnGuardar.setEnabled(false);
                	
                }catch (Exception e2) {
        			// TODO Auto-generated catch block
        			e2.printStackTrace();
        		}	
                	
                //System.out.println(txtNombre.getText());
			}
		});
			

	}
}


	
