package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import Main_Comun_App.SQL;

import java.awt.Font;
import java.awt.Graphics2D;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.SystemColor;

public class AdministradorListaSuperUsuarios extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministradorListaSuperUsuarios frame = new AdministradorListaSuperUsuarios();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdministradorListaSuperUsuarios() {
		
		
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setFont(new Font("Arial", Font.PLAIN, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 250, 250));
		panel.setBounds(0, 0, 784, 562);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCuenta = DefaultComponentFactory.getInstance().createTitle("Cuenta");
		lblCuenta.setForeground(Color.WHITE);
		lblCuenta.setBounds(141, 37, 88, 14);
		panel.add(lblCuenta);
		lblCuenta.setFont(new Font("Arial", Font.BOLD, 12));
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(18, 11, 113, 132);
		panel.add(label_1);
		label_1.setVerticalAlignment(SwingConstants.TOP);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		JLabel lblAdmin = DefaultComponentFactory.getInstance().createTitle("Admin");
		lblAdmin.setForeground(Color.WHITE);
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 16));
		lblAdmin.setBounds(141, 21, 88, 14);
		panel.add(lblAdmin);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(UIManager.getBorder("RadioButton.border"));
		panel_1.setBounds(65, 76, 635, 486);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblListaDeUsuarios = DefaultComponentFactory.getInstance().createTitle("Lista de SuperUsuarios:");
		lblListaDeUsuarios.setFont(new Font("Arial", Font.BOLD, 17));
		lblListaDeUsuarios.setBounds(10, 11, 201, 14);
		panel_1.add(lblListaDeUsuarios);
		
		JLabel superusuario = DefaultComponentFactory.getInstance().createLabel("SuperUser");
		superusuario.setFont(new Font("Arial", Font.BOLD, 12));
		superusuario.setBounds(87, 110, 72, 14);
		panel_1.add(superusuario);
		
		JLabel lblNombre = DefaultComponentFactory.getInstance().createTitle("SuperUsuarios");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 12));
		lblNombre.setBounds(77, 85, 88, 14);
		panel_1.add(lblNombre);
		
		JRadioButton radioButton = new JRadioButton("SuperUser");
		radioButton.setBackground(Color.WHITE);
		radioButton.setSelected(true);
		radioButton.setEnabled(false);
		radioButton.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton.setBounds(240, 106, 88, 23);
		panel_1.add(radioButton);
		
		JLabel label_2 = new JLabel("Rol Actual");
		label_2.setFont(new Font("Arial", Font.BOLD, 12));
		label_2.setBounds(272, 85, 58, 14);
		panel_1.add(label_2);
		
		JRadioButton radioButton_1 = new JRadioButton("Admin");
		radioButton_1.setBackground(Color.WHITE);
		radioButton_1.setEnabled(false);
		radioButton_1.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_1.setBounds(173, 106, 61, 23);
		panel_1.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("Estandar");
		radioButton_2.setBackground(Color.WHITE);
		radioButton_2.setEnabled(false);
		radioButton_2.setFont(new Font("Arial", Font.PLAIN, 12));
		radioButton_2.setBounds(332, 105, 84, 23);
		panel_1.add(radioButton_2);
		
		JButton btnVer = new JButton("Ver");
		btnVer.setBounds(422, 106, 61, 23);
		panel_1.add(btnVer);
		
		JButton button_1 = new JButton("Borrar");
		button_1.setBounds(493, 106, 72, 23);
		panel_1.add(button_1);
		
		JLabel label_3 = new JLabel("Opciones");
		label_3.setFont(new Font("Arial", Font.BOLD, 12));
		label_3.setBounds(457, 81, 61, 14);
		panel_1.add(label_3);
		
		JLabel lblNewJgoodiesTitle = DefaultComponentFactory.getInstance().createTitle("Journey & Experience");
		lblNewJgoodiesTitle.setForeground(Color.WHITE);
		lblNewJgoodiesTitle.setBounds(503, 11, 221, 32);
		panel.add(lblNewJgoodiesTitle);
		lblNewJgoodiesTitle.setFont(new Font("Arial", Font.BOLD, 20));
	
		
		//BOTON ATRAS
		JLabel btnAtras = new JLabel("");
		btnAtras.setIcon(new ImageIcon(AdministradorCuenta.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setBounds(710, 92, 51, 51);
		panel.add(btnAtras);
		
		SQL db=new SQL();
		//LLAMAMOS LOS VALORES DE LA BASE DE DATOS A LOS TEXTFIELD----------------------------------
		try {
			db.SQLConnection("grupo1","root","");
		    
			superusuario.setText(db.getValuesUsuario("revenger")[0]);
			
			//IMAGEN FONDO
			
			JLabel ImagenFondo = new JLabel("");
			ImagenFondo.setIcon(new ImageIcon(AdministradorVerUsuario.class.getResource("/Vistas/wallpaper.jpg")));
			ImagenFondo.setBounds(0, 0, 784, 562);
			panel.add(ImagenFondo);

		
			db.closeConnection();
			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}		
		//-------------------------------------------------------------------------------------------		
		//LISTENER BOTON VER
		
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
			
			AdministradorVerSuperUsuario ver = new AdministradorVerSuperUsuario();
			ver.setVisible(true);
			dispose();
			
			}
		});

		//LISTENER DE LABEL ATRAS
		MouseListener patras =new MouseListener() {
			  
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
					AdministradorPantallaPrincipal v= new AdministradorPantallaPrincipal();
					v.setVisible(true);
					dispose();
				   
				   }
			};
			btnAtras.addMouseListener(patras);	
		//LISTENER DE LABEL CUENTA
		MouseListener cuenta =new MouseListener() {
			   
			  public void mouseReleased(MouseEvent arg0) { }	   
			  public void mousePressed(MouseEvent arg0) {  }		   
			  public void mouseExited(MouseEvent arg0) {  }
			  public void mouseEntered(MouseEvent arg0) { }
			  
			 public void mouseClicked(MouseEvent arg0) {
						  
				   AdministradorCuenta cuenta=new AdministradorCuenta();
				   cuenta.setVisible(true);				   }
			};
			lblCuenta.addMouseListener(cuenta);
	}
}
