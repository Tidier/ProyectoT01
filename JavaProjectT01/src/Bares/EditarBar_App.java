package Bares;


import java.awt.BorderLayout;



import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Pais.EliminarPaisApp;
import WipAssiya.SQL;

import Usuario.EspacioPersonalApp;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JTextArea;

public class EditarBar_App extends JFrame {

	private JPanel contentPane;
	private static Connection Conexion = null;
	private String  db_name="";
	private String  user="grupo1";
	private String   pass="root";
	private JTextField txtnombre;
	private JTextField txtpais;
	private JTextField txtciudad;
	private JTextField txtcontacto;
	private JTextField txtespecialdiad;
	private JTextField texttarifa;
	int Id_Bar=0;
	
	SQL bd=new SQL();
	private JTextField txtHorario;
	


	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MostrarHotel_App frame = new MostrarHotel_App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public EditarBar_App() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 594, 567);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(169, 169, 169));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(new Color(0, 0, 0));
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(10, 27, 335, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(EditarBar_App.class.getResource("/Vistas/iconoPrincipal.jpg")));
		label_1.setBounds(318, 11, 156, 76);
		contentPane.add(label_1);
		
		JLabel btnAtras = new JLabel("");
		btnAtras.setForeground(Color.BLUE);
		btnAtras.setIcon(new ImageIcon(EditarBar_App.class.getResource("/Vistas/flechaAtras.png")));
		btnAtras.setFont(new Font("Arial", Font.BOLD, 11));
		btnAtras.setBounds(37, 454, 68, 51);
		contentPane.add(btnAtras);
;
		
        JLabel lblCiudadQueQuieres = new JLabel("Selecciona el bar");
        lblCiudadQueQuieres.setFont(new Font("Arial", Font.BOLD, 11));
        lblCiudadQueQuieres.setBounds(37, 132, 174, 14);
        contentPane.add(lblCiudadQueQuieres);
		
		final JComboBox ListaRestaurante = new JComboBox();
		ListaRestaurante.setBounds(245, 129, 140, 20);
		contentPane.add(ListaRestaurante);
	
//..........................codigo para obtener hoteles de BD y a�adirlos a combox...........................................//

		//SQL bd=new SQL();

		try {
				bd.SQLConnection("grupo1","root","");
				String rellenar[]=bd.getValuesBares();
		
				for(int i=0;i<rellenar.length;i++)
				{
					ListaRestaurante.addItem(rellenar[i]);		
				}
		} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
		}
		
		
		final JLabel lblDatosDelHotel = new JLabel("Datos del bar");
		lblDatosDelHotel.setFont(new Font("Arial", Font.BOLD, 12));
		lblDatosDelHotel.setBounds(37, 178, 120, 14);
		contentPane.add(lblDatosDelHotel);
		lblDatosDelHotel.setVisible(false);
		
		final JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(106, 215, 79, 14);
		contentPane.add(lblNombre);
		lblNombre.setVisible(false);
		
		final JLabel lblPais = new JLabel("Pais");
		lblPais.setFont(new Font("Arial", Font.BOLD, 11));
		lblPais.setBounds(106, 240, 68, 14);
		contentPane.add(lblPais);
		lblPais.setVisible(false);

		
		final JLabel lblCiudad = new JLabel("Ciudad");
		lblCiudad.setFont(new Font("Arial", Font.BOLD, 11));
		lblCiudad.setBounds(106, 265, 68, 14);
		contentPane.add(lblCiudad);
		lblCiudad.setVisible(false);
		

		
		final JLabel lblContacto = new JLabel("Contacto");
		lblContacto.setFont(new Font("Arial", Font.BOLD, 11));
		lblContacto.setBounds(106, 290, 68, 14);
		contentPane.add(lblContacto);
		lblContacto.setVisible(false);
		
		final JLabel lblServicios = new JLabel("Especialidad");
		lblServicios.setFont(new Font("Arial", Font.BOLD, 11));
		lblServicios.setBounds(106, 315, 79, 14);
		contentPane.add(lblServicios);
		lblServicios.setVisible(false);
	
		
		final JLabel lblTarifas = new JLabel("Precio");
		lblTarifas.setFont(new Font("Arial", Font.BOLD, 11));
		lblTarifas.setBounds(106, 340, 68, 14);
		contentPane.add(lblTarifas);
		lblTarifas.setVisible(false);

		
		final JLabel lblDescripcin = new JLabel("Descripci\u00F3n");
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcin.setBounds(106, 403, 89, 14);
		contentPane.add(lblDescripcin);
		lblDescripcin.setVisible(false);

		
		txtnombre = new JTextField();
		txtnombre.setEditable(false);
		txtnombre.setBounds(212, 210, 133, 20);
		contentPane.add(txtnombre);
		txtnombre.setColumns(10);
		txtnombre.setVisible(false);
	
		
		txtpais = new JTextField();
		txtpais.setEditable(false);
		txtpais.setColumns(10);
		txtpais.setBounds(212, 237, 133, 20);
		contentPane.add(txtpais);
		txtpais.setVisible(false);
	
		
		txtciudad = new JTextField();
		txtciudad.setEditable(false);
		txtciudad.setColumns(10);
		txtciudad.setBounds(212, 262, 133, 20);
		contentPane.add(txtciudad);
		txtciudad.setVisible(false);

		
		txtcontacto = new JTextField();
		txtcontacto.setEditable(false);
		txtcontacto.setColumns(10);
		txtcontacto.setBounds(212, 287, 133, 20);
		contentPane.add(txtcontacto);
		txtcontacto.setVisible(false);

		
		txtespecialdiad = new JTextField();
		txtespecialdiad.setEditable(false);
		txtespecialdiad.setColumns(10);
		txtespecialdiad.setBounds(212, 312, 133, 20);
		contentPane.add(txtespecialdiad);
		txtespecialdiad.setVisible(false);

		
		
		texttarifa = new JTextField();
		texttarifa.setEditable(false);
		texttarifa.setColumns(10);
		texttarifa.setBounds(212, 337, 133, 20);
		contentPane.add(texttarifa);
		texttarifa.setVisible(false);

		
		final JTextArea txtdescripcion = new JTextArea();
		txtdescripcion.setEditable(false);
		txtdescripcion.setBounds(212, 398, 240, 69);
		contentPane.add(txtdescripcion);
		
		final JLabel lblAdmin = new JLabel("Administrador");
		lblAdmin.setFont(new Font("Arial", Font.BOLD, 11));
		lblAdmin.setBounds(328, 179, 97, 14);
		contentPane.add(lblAdmin);
		lblAdmin.setVisible(false);
		
		final JComboBox ListaAdmin = new JComboBox();
		ListaAdmin.setBounds(424, 176, 121, 20);
		contentPane.add(ListaAdmin);
		ListaAdmin.setVisible(false);	
		
		final JButton btnNewButton_1 = new JButton("Editar");
	
		btnNewButton_1.setFont(new Font("Arial", Font.BOLD, 11));
		btnNewButton_1.setBounds(456, 477, 89, 23);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.setVisible(false);
		
		final JComboBox ListaPais = new JComboBox();
		ListaPais.setBounds(375, 237, 134, 20);
		contentPane.add(ListaPais);
		ListaPais.setVisible(false);
		
		final JComboBox ListaCiudad = new JComboBox();
		ListaCiudad.setBounds(375, 262, 134, 20);
		contentPane.add(ListaCiudad);
		
		final JLabel lblNewLabel = new JLabel("Horario");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(106, 367, 68, 14);
		contentPane.add(lblNewLabel);
		lblNewLabel.setVisible(false);
		
		txtHorario = new JTextField();
		txtHorario.setEditable(false);
		txtHorario.setBounds(212, 367, 133, 20);
		contentPane.add(txtHorario);
		txtHorario.setColumns(10);
		txtHorario.setVisible(false);
		txtHorario.setEditable(false);
		
		JButton btnNewButton_2 = new JButton("Buscar\r\n");
		

		
		btnNewButton_2.setFont(new Font("Arial", Font.BOLD, 11));
		btnNewButton_2.setBounds(420, 128, 89, 23);
		contentPane.add(btnNewButton_2);
		ListaCiudad.setVisible(false);
		txtdescripcion.setVisible(false);

		
		
		//......................obtener los nombre de los admin. para identificar quien admin regisro el hotel....................
		
		
			String rellenarAdmin[]=bd.getValues_usuaroAdministrador();
			  
			
			for(int i=0;i<rellenarAdmin.length;i++)
			{
				ListaAdmin.addItem(rellenarAdmin[i]);
				
			}
	//-------------------------------------------------------FIN CODIGO DE OBTENER admin.........................................		


		//........................................ codigo para obtener todos los paises de BD.................................
				try {
					bd.SQLConnection("grupo1","root","");
					String rellenar[]=bd.getValuesPais();
				
						for(int i=0;i<rellenar.length;i++)
					{
					   ListaPais.addItem(rellenar[i]);		
					}
					//bd.closeConnection();
			    } catch (Exception e2) {
				// TODO Auto-generated catch block
			    	e2.printStackTrace();
			    }
				
		//........................................ FIN codigo para obtener todos los paises de BD.................................
	
				//......................obtener los nombre de las ciudades. k estan en BD....................
				
				try {
					bd.SQLConnection("grupo1","root","");
					String rellenar[]=bd.getValuesCiudad();
					
					for(int i=0;i<rellenar.length;i++)
					{
						ListaCiudad.addItem(rellenar[i]);
						
					}
					bd.closeConnection();
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
		//-------------------------------------------------------FIN CODIGO DE OBTENER ciudad.........................................		

		

				 
				 
//.................................... evento para mostrar datos del hotel selecionada......................................	
	
							
								
							
//.................................... evento para guardar los cambios del hotel selecionado......................................	

					btnNewButton_1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
							     String pais=ListaPais.getSelectedItem().toString();
					            String dniAdmin=ListaAdmin.getSelectedItem().toString();
							    String nom_ciudad= ListaCiudad.getSelectedItem().toString();
					          
						      int Contacto=Integer.parseInt(txtcontacto.getText()); // pasamos contacto a int
						      int precio=Integer.parseInt(texttarifa.getText()); // pasamos contacto a int

					            
						          String dni=bd.Obtener_Dni_Admin(dniAdmin); //obtener el dni de admin
								int cod_ciudad=bd.ObtenerCod_Ciudad(nom_ciudad); // obtener el codigo de ciudad


							SQL bd=new SQL();
							try {
								bd.SQLConnection("grupo1","root","");
								bd.UpdatedeleteRecord_Bar(Id_Bar,txtnombre.getText().toString(), Contacto, txtespecialdiad.getText().toString(),precio,txtHorario.getText().toString(), txtdescripcion.getText().toString());
								bd.UpdatedeleteRecord_TenerBar(Id_Bar, cod_ciudad, pais, dni);
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});			
	
//.................................... FIN evento para guardar los cambios del hotel selecionado......................................	


					
					btnNewButton_2.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							
							
							//SQL bd=new SQL();
						      //int Contacto=Integer.parseInt(txtContacto.getText()); // pasamos contacto a int

							 
							 try {
								 String Bar= ListaRestaurante.getSelectedItem().toString();

								 String QueryResta="SELECT * FROM " + "BARES"+ " WHERE NOMBRE= '"+Bar+"'";

								 Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								 Statement st=Conexion.createStatement();
								 java.sql.ResultSet ResultSet;
								 ResultSet= st.executeQuery(QueryResta);
								 
								 while (ResultSet.next())
								 {
									txtnombre.setText(ResultSet.getString("Nombre"));
									txtcontacto.setText(ResultSet.getString("Contacto"));
									txtespecialdiad.setText(ResultSet.getString("Especialidad"));
									txtdescripcion.setText(ResultSet.getString("Descripcion"));
									texttarifa.setText(ResultSet.getString("Precio"));
									txtHorario.setText(ResultSet.getString("Horario"));
									Id_Bar=ResultSet.getInt("Id_bar");

								 }
								 
							 }catch (Exception e2) {
										// TODO Auto-generated catch block
										e2.printStackTrace();
									}
								 

				    try {
							// String restaurante= ListaRestaurante.getSelectedItem().toString();
							
							  bd.SQLConnection("grupo1","root","");


								String QueryPais="SELECT * FROM " + "Tener_bar"+ " WHERE Id_bar="+Id_Bar;
								Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
								Statement st=Conexion.createStatement();
								java.sql.ResultSet ResultSet;
								ResultSet= st.executeQuery(QueryPais);
									 
								while (ResultSet.next())
								 {
									String  nom_ciudad=bd.getValues_NombreCiudad(ResultSet.getInt("id_ciudad"));
									txtciudad.setText(nom_ciudad);
									txtpais.setText(ResultSet.getString("Nombre_Pais"));

								 }
					
					} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
					}
							
								// hacer visible los campos para que el admin. modifique los datos de la ciudad selecionada
								txtdescripcion.setVisible(true);
								txtdescripcion.setEditable(true);
								texttarifa.setVisible(true);
								texttarifa.setEditable(true);
								txtespecialdiad.setVisible(true);
								txtespecialdiad.setEditable(true);
								txtcontacto.setVisible(true);
								txtcontacto.setEditable(true);
								txtciudad.setVisible(true);
								txtpais.setVisible(true);
								txtnombre.setVisible(true);
								txtnombre.setEditable(true);
								lblDescripcin.setVisible(true);
								lblTarifas.setVisible(true);
								lblServicios.setVisible(true);
								lblCiudad.setVisible(true);
								lblPais.setVisible(true);
								lblDatosDelHotel.setVisible(true);
								lblNombre.setVisible(true);
								lblContacto.setVisible(true);
								lblAdmin.setVisible(true);
								ListaAdmin.setVisible(true);
								btnNewButton_1.setVisible(true);
								ListaPais.setVisible(true);
								ListaCiudad.setVisible(true);
								txtHorario.setVisible(true);
								txtHorario.setEditable(true);
								lblNewLabel.setVisible(true);

						}
					});
					bd.closeConnection();
					
					
					// evento a clicar sobre label,atras.
					
					 MouseListener clikLabel=new MouseListener() {
							   
					  public void mouseReleased(MouseEvent arg0) { }	   
					  public void mousePressed(MouseEvent arg0) {  }		   
					  public void mouseExited(MouseEvent arg0) {  }
					  public void mouseEntered(MouseEvent arg0) { }
					  
					 public void mouseClicked(MouseEvent arg0) {
								  
						   GestionBar atras=new GestionBar();
						   atras.setVisible(true);				   }
					};
					 btnAtras.addMouseListener(clikLabel);
					 
					
	}
}

