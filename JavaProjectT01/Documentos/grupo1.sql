create table Administradores
(
  Dni varchar2(9) not null,
  clave varchar2(10)  not null UNIQUE,
  usuario varchar2(10)  not null UNIQUE,
  Nombre varchar2(50)  constraint nombre_Nn not null,
  Apellido varchar2(50)  constraint Apellido_Nn not null,
  foto blob,
  dni_superusuario varchar2(9),
  
  constraint DniAdmin_pk primary key (Dni)
  

);

  alter table Administradores add foreign key (dni_superusuario) references Administradores (Dni);


create table Usuarios
(
  Usuario varchar2(20)constraint usuarioUsuario_Nn not null,
  Clave varchar(10) constraint claveUsuario_Nn not null,
  Nombre varchar2(50) constraint NombreUsuario_Nn not null,
  Apellido varchar2(50) constraint ApellidoUsuario_Nn not null,
  Email varchar(100) constraint EmailUsuario_Nn not null,
  Codigo_postal int,
  Ultimo_viaje varchar2(200),
  Pais_destino varchar2 (300),
  Otros_datos varchar2(300),
  Foto blob,
  constraint usuari_pk primary key (Usuario,Clave),
  constraint claveUsuario_Uk unique (clave,usuario)
);



create table Paises
(
  Nombre varchar2(200) constraint NombrePais_Nn not null,
  Moneda varchar2(200) constraint MonedaPais_Nn not null,
  Idioma varchar2(200) constraint IdiomaPais_Nn not null,
  Bandera varchar2(200) constraint BanderaPais_Uk unique,
  Capital varchar2(200) constraint CapitalPais_Nn not null,
  Habitantes int constraint HabitantePais_Nn not null,
  Nombre_cont varchar2(200)constraint ContinentePais_Nn not null,

  constraint nom_pk primary key (Nombre)

);

/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Ciudades
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;
create table Ciudades_Pueblos
(
  id_ciudad int,
  Nombre varchar2(200)constraint NombreCiudad_Nn not null,
  descripcion varchar2(300),
  Nombre_pais varchar2(200),
  constraint nomPa_pk primary key (id_ciudad),
  constraint NombrePais_fk foreign key (Nombre_pais) references Paises(Nombre)
);


/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Hoteles
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Hoteles
(
    Id_hotel int,
    Nombre varchar2(250) constraint NombreHotel_Nn not null,
    Contacto int constraint ContacteHotel_Nn not null, 
    Servicios varchar2(200) constraint ServiciosHotel_Nn not null, 
    Tarifas varchar2(100), 
    Descripcion varchar2(200) constraint DescripcionHotel_Nn not null,
    Foto blob,
    constraint hotel_pk primary key(Id_hotel)
);

Create table Tener_hotel
(
  Id_hotel int,
  id_ciudad int,
  
  constraint hot_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
  constraint ciudad_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_hotel add constraint hot_ciudad_pk primary key (Id_hotel,id_ciudad);
  
create table Opinar_hotel
(
  Id_hotel int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300) constraint opinarHotel_Nn not null,
  data_comentario date,
  
  constraint hotel_user_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
  constraint opinarUsuario_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_hotel add constraint op_hot_user_pk primary key (Id_hotel,Clave,Usuario);


/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_restaurantes
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Restaurantes
(
  Id_restaurante int,
  Nombre varchar2(200) constraint NombreRestaurante_Nn not null,
  Contacto int constraint ContactoRestaurante_Nn not null,
  Especialidad varchar2(200) constraint EspecialidadRestaurante_Nn not null,
  Tarifas varchar2(200), 
  Horario varchar2(200) constraint HorarioRestaurante_Nn not null, 
  Descripcion varchar2(200) constraint DescripcionrRestaurante_Nn not null,
  foto blob,
  
  constraint res_pk primary key(Id_restaurante)
);

 
create table Tener_restaurante
(
  Id_restaurante int,
  id_ciudad int,
  
  constraint res_user_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint ciudad_resta_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_restaurante add constraint entre_ciudad_pk primary key (Id_restaurante,id_ciudad);
  
  
  create table Opinar_restuarante
(
  Id_restaurante int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300) constraint OpinarRestaurante_Nn not null,
  data_comentario date,
  
  constraint res_user_op_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint opinar_Usuario_rest_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_restuarante add constraint op_resta_user_pk primary key ( Id_restaurante,Clave,Usuario);

/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Bares
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

create table Bares
(
  Id_bar int,
  Nombre varchar2(100)constraint NombreBar_Nn not null,
  Contacto int constraint ContactoBar_Nn not null,
  Especialidad varchar2(100) constraint EspecialidadBar_Nn not null, 
  Precio int constraint precioBar_Nn not null,
  Horario varchar2(50) constraint HorarioBar_Nn not null,
  Descripcion varchar2(200)constraint DescripcionBar_Nn not null,
  foto blob,
 
  constraint barPais_pk primary key (Id_bar)
);

create table Tener_bar
(
  Id_bar int,
  id_ciudad int,
  
  constraint bares_user_op_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint ciudad_bar_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_bar add constraint bar_ciudad_pk primary key (Id_bar,id_ciudad);
  
  create table Opinar_bar
(
  Id_bar int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint bar_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint opinarUsuario_bar_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_bar add constraint op_bar_user_pk primary key ( Id_bar,Clave,Usuario);
  
/* sequencia para crear id automatico*/
CREATE SEQUENCE SEQ_Entretenimiento
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

Create table Entretenimiento
(
  Id_entre int,
  Tipo varchar2(200) constraint TipoEnter_Nn not null,
  Descripcion varchar2(200) constraint descripcionEnter_Nn not null, 
  Horario varchar2(200) constraint horarioEnter_Nn not null,
  Contacto int constraint ContactoEnter_Nn not null,
  foto blob,

  constraint entret_pk primary key(Id_entre)
);

create table Tener_entretenimiento
(
  Id_entre int,
  id_ciudad int,
  constraint enter_user_op_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint ciudad_entre_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_entretenimiento add constraint entre_2ciudad_pk primary key (Id_entre,id_ciudad);
  
  
 create table Opinar_entretenimiento
(
  Id_entre int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint enter_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint opinarUsuario_entre_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_entretenimiento add constraint op_entre_user_pk primary key ( Id_entre,Clave,Usuario);
  
CREATE SEQUENCE SEQ_servicios
MINVALUE 1
START WITH 1
INCREMENT BY 1
CACHE 1000000;

create table Servicios 
(
  Id_servicio int,
  Tipo varchar2(200) constraint TipoServicio_Nn not null,
  descripcion varchar2(300) constraint DescripcionServicio_Nn not null,
  Contacto int constraint ContactoServicio_Nn not null,
  horario varchar2(100) constraint HorarioServicio_Nn not null,
  foto blob,
  
  constraint servicio_pk primary key(Id_servicio)
);

create table Tener_servicios
(
  id_servicio int,
  id_ciudad int,
  
  constraint serv_user_fk1 foreign key (id_servicio) references Servicios (Id_servicio),
  constraint ciudad_ser_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_servicios add constraint serv_ciudad_pk primary key (Id_servicio,id_ciudad);


 create table Opinar_servicios
(
  Id_servicio,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint serv_user_op_fk1 foreign key (Id_servicio) references Servicios (Id_servicio),
  constraint opinarUsuario_serv_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_servicios add constraint op_serv_user_pk primary key ( Id_servicio,Clave,Usuario);