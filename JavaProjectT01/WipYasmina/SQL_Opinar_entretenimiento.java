package EntornoGrafico;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SQL {
	
	private static Connection Conexion = null;


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		public void createTable(String Opinar_entretenimiento) {
		        try {
		            String Query = "CREATE TABLE " + Opinar_entretenimiento + ""
		                    + "(ID_ENTRE VARCHAR(25), USUARIO VARCHAR(50), OPINION VARCHAR(50),"
		                    + " DATA_COMENTARIO DATE)";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + Opinar_entretenimiento + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		        }
		    }
		

	 //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertData(String Opinar_entretenimiento, String ID_ENTRE, String USUARIO, String OPINION, String DATA_COMENTARIO) {
		        try {
		            String Query = "INSERT INTO " + Opinar_entretenimiento + " VALUES("
		            		+ "'"+ ID_ENTRE + "',"
		            		+ "'"+ USUARIO + "',"
		            		+ "'"+ OPINION + "',"
		            		+ "'"+ DATA_COMENTARIO + "',"
		           
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
		 
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValuesOpinarEntretenimiento() {
		        try {
		            String Query = "SELECT * FROM Opinar_entretenimiento";
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("ID_ENTRE: " + resultSet.getString("ID_ENTRE") + " "
		                        + "USUARIO: " + resultSet.getString("USUARIO") + " " 
								+ resultSet.getString("OPINION") + " "
		                        + "DATA_COMENTARIO: " + resultSet.getString("FECHA_COMENTARIO") + " ");
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
		 
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(String Opinar_entretenimiento, String ID_ENTRE) {
		        try {
		            String Query = "DELETE FROM " + Opinar_entretenimiento + " WHERE ID = '" + ID_ENTRE + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }

	

}
