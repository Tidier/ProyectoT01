package EntornoGrafico;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SQL {
	
	private static Connection Conexion = null;


	//METODO DE CONEXION A SQL
	public void SQLConnection(String user, String pass, String db_name) throws Exception {
	        try {
	            Class.forName("oracle.jdbc.driver.OracleDriver");
	            Conexion = DriverManager.getConnection("jdbc:oracle:thin:@localhost" + db_name, user, pass);
	            JOptionPane.showMessageDialog(null, "Se ha iniciado la conexi�n con el servidor de forma exitosa");
	        } catch (ClassNotFoundException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        } catch (SQLException ex) {
	            JOptionPane.showMessageDialog(null, "NO Se ha iniciado la conexi�n con el servidor");
	        }
	    }
	
	//METODO QUE FINALIZA LA CONEXION A SQL
		public void closeConnection() {
		        try {
		            Conexion.close();
		            JOptionPane.showMessageDialog(null, "Se ha finalizado la conexi�n con el servidor");
		        } catch (SQLException ex) {
		           
		            JOptionPane.showMessageDialog(null, "NO Se ha finalizado la conexi�n con el servidor");
		        }
		    }
		
	//METODO QUE CREA TABLA EN NUESTRA BASE DE DATOS	
		public void createTable(String Servicios) {
		        try {
		            String Query = "CREATE TABLE " + Servicios + ""
		                    + "(ID_SERVICIO VARCHAR(25),TIPO VARCHAR(50), DESCRIPCION VARCHAR(50),"
		                    + " CONTACTO VARCHAR(3), HORARIO VARCHAR(1), IMAGEN.JPG)";

		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Se ha creado la tabla " + Servicios + " de forma exitosa");
		        } catch (SQLException ex) {
		            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
		        }
		    }
		

	 //METODO QUE INSERTA VALORES EN NUESTRA BASE DE DATOS
		 public void insertData(String Servicios, String ID_SERVICIO, String TIPO, String DESCRIPCION, String CONTACTO, String HORARIO, String IMAGEN.JPG) {
		        try {
		            String Query = "INSERT INTO " + Servicios + " VALUES("
		            		+ "'"+ ID_SERVICIO + "',"
		            		+ "'"+ TIPO + "',"
		            		+ "'"+ DESCRIPCION + "',"
		            		+ "'"+ CONTACTO + "',"
		            		+ "'"+ HORARIO + "',"
		            		+ "'"+ IMAGEN.JPG + "')";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);
		            JOptionPane.showMessageDialog(null, "Datos almacenados de forma exitosa");
		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en el almacenamiento de datos");
		        }
		    }	
		 
		 
	//METODO QUE OBTIENE VALORES DE NUESTRA BASE DE DATOS		
		 public void getValues(String Servicios) {
		        try {
		            String Query = "SELECT * FROM " + Servicios;
		            Statement st = Conexion.createStatement();
		            java.sql.ResultSet resultSet;
		            resultSet = st.executeQuery(Query);

		            while (resultSet.next()) {
		                System.out.println("ID_SERVICIO: " + resultSet.getString("ID_SERVICIO") + " "
		                        + "TIPO: " + resultSet.getString("TIPO") + " " 
								+ resultSet.getString("DESCRIPCION") + " "
		                        + "CONTACTO: " + resultSet.getString("CONTACTO") + " "
		                        + "HORARIO: " + resultSet.getString("HORARIO") + " "
		                        + "IMAGEN.JPG: " + resultSet.getString("IMAGEN.JPG"));
		            }

		        } catch (SQLException ex) {
		            JOptionPane.showMessageDialog(null, "Error en la adquisici�n de datos");
		        }
		    }
		 
	//METODO QUE ELIMINA VALORES DE NUESTRA BASE DE DATOS	
		 public void deleteRecord(String Servicios, String ID_SERVICIO) {
		        try {
		            String Query = "DELETE FROM " + Servicios + " WHERE ID = '" + ID_SERVICIO + "'";
		            Statement st = Conexion.createStatement();
		            st.executeUpdate(Query);

		        } catch (SQLException ex) {
		            System.out.println(ex.getMessage());
		            JOptionPane.showMessageDialog(null, "Error borrando el registro especificado");
		        }
		    }

	

}
