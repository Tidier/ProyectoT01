package EntornoGrafico;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Tablapais extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tablapais frame = new Tablapais();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tablapais() {
		setTitle("Pais");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 581, 592);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Journey&Experience");
		label.setForeground(Color.BLUE);
		label.setFont(new Font("Arial Black", Font.ITALIC, 25));
		label.setBounds(71, 47, 364, 43);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("New label");
		label_1.setBounds(425, 29, 134, 76);
		contentPane.add(label_1);
		
		JLabel lblNewLabel = new JLabel("Datos del pais");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 11));
		lblNewLabel.setBounds(38, 158, 97, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(38, 201, 67, 14);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(115, 198, 121, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblHabitantes = new JLabel("Habitantes");
		lblHabitantes.setBounds(38, 294, 67, 14);
		contentPane.add(lblHabitantes);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(115, 229, 121, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(115, 260, 121, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(115, 291, 121, 20);
		contentPane.add(textField_3);
		
		JLabel lblContinente = new JLabel("Continente");
		lblContinente.setBounds(38, 263, 63, 14);
		contentPane.add(lblContinente);
		
		JLabel label_2 = new JLabel("Idioma");
		label_2.setBounds(38, 325, 67, 14);
		contentPane.add(label_2);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(115, 322, 121, 20);
		contentPane.add(textField_4);
		
		JLabel lblCapital = new JLabel("Capital");
		lblCapital.setBounds(38, 232, 67, 14);
		contentPane.add(lblCapital);
		
		JLabel lblMoneda = new JLabel("Moneda");
		lblMoneda.setBounds(38, 356, 67, 14);
		contentPane.add(lblMoneda);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(115, 353, 121, 20);
		contentPane.add(textField_5);
		
		JLabel lblBandera = new JLabel("Bandera");
		lblBandera.setBounds(38, 408, 67, 14);
		contentPane.add(lblBandera);
		
		JLabel lblNewLabel_2 = new JLabel("imagen\r\n");
		lblNewLabel_2.setForeground(SystemColor.controlLtHighlight);
		lblNewLabel_2.setBackground(SystemColor.text);
		lblNewLabel_2.setBounds(115, 384, 121, 63);
		contentPane.add(lblNewLabel_2);
		
		// a�adir los datos del pais a bd
		JButton btnNewButton = new JButton("A\u00F1adir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(362, 226, 89, 23);
		contentPane.add(btnNewButton);
		// modificar los datos de pais, pero antes de que nos salga el pais debemos de programar  un filtro para escogir el paisk queremos modificar
		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModificar.setBounds(362, 263, 89, 23);
		contentPane.add(btnModificar);
		
		// Eliminar los datos de pais, pero antes de que nos salga el pais debemos de programar  un filtro para escogirr el paisk queremos eliminar
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEliminar.setBounds(362, 297, 89, 23);
		contentPane.add(btnEliminar);
		
		// mostrar los datos de pais, pero antes de que nos salga el pais, debemos de programar  un filtro para escogir el pais k queremos mostrar
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnMostrar.setBounds(362, 331, 89, 23);
		contentPane.add(btnMostrar);
		
		//guardara los cambios a base de datos.
		JButton btnNewButton_1 = new JButton("Guardar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(225, 424, 89, 23);
		contentPane.add(btnNewButton_1);
	}
}
