import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window frame = new Window();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Window() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 421, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Registrar Usuario");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(22, 79, 115, 23);
		contentPane.add(btnNewButton);
		
		JButton btnObtenerRegistros = new JButton("Obtener registros");
		btnObtenerRegistros.setBounds(155, 24, 128, 23);
		contentPane.add(btnObtenerRegistros);
		
		JButton btnBorrarRegistro = new JButton("Borrar registro");
		btnBorrarRegistro.setBounds(274, 79, 121, 23);
		contentPane.add(btnBorrarRegistro);
		
		JButton button_2 = new JButton("New button");
		button_2.setBounds(167, 167, 89, 23);
		contentPane.add(button_2);
	}
}
