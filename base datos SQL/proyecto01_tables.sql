create table Administradores
(
  clave varchar2(10),
  usuario varchar2(10),
  Dni varchar2(9),
  Nombre varchar2(50),
  Apellido varchar2(50),
  foto blob,
  dni_superusuario varchar2(9),
  
  constraint clave_pk primary key (Dni)
);

  alter table Administradores add foreign key (dni_superusuario) references Administradores (Dni);


create table Usuarios
(
  Usuario varchar2(20),
  Clave varchar(10),
  Nombre varchar2(50),
  Apellido varchar2(50),
  Email varchar(100),
  Codigo_postal int,
  Comentarios varchar2(250),
  Dni_admin varchar2(9),
  Ultimo_viaje varchar2(200),
  Pais_destino varchar2 (300),
  otros_datos varchar2(300
  foto blob,
  constraint usuari_pk primary key (Usuario,Clave),
  constraint adm_fk foreign key (Dni_admin) references Administradores(Dni)
);



create table Paises
(
  Nombre varchar2(200),
  Moneda varchar2(200),
  Idioma varchar2(200),
  Bandera varchar2(200),
  Capital varchar2(200),
  Habitantes int,
  Nombre_cont varchar2(200),
  
  constraint nom_pk primary key (Nombre)

);

create table Ciudades_Pueblos
(
  Nombre varchar2(200),
  Nombre_pais varchar2(200),
  capital varchar2(200),
    
    constraint nomPa_pk primary key (Nombre),
    constraint paisNom_fk foreign key (Nombre_pais) references Paises(Nombre)
);

Create table Hoteles
(
    Id_hotel int,
    Nombre varchar2(250),
    Contacto int, 
    Servicios varchar2(200), 
    Tarifas varchar2(100), 
    Descripcion varchar2(200),
    Opinion_usuario varchar2(300),
    
    constraint hotel_pk primary key(Id_hotel)
);

Create table Tener_hotel
(
  Id_hotel int,
  Nombre_ciudad varchar2(200),
  
  constraint hot_fk1 foreign key (id_hotel) references Hoteles (Id_hotel),
  constraint ciudad_fk2 foreign key (Nombre_ciudad) references  Ciudades_Pueblos(Nombre)
);

  alter table Tener_hotel add constraint hot_ciudad_pk primary key (Id_hotel,Nombre_ciudad);
  

Create table Restaurantes
(
  Id_restaurante int,
  Nombre varchar2(200),
  Contacto int,
  Especialidad varchar2(200),
  Tarifas varchar2(200), 
  Horario varchar2(200), 
  Descripcion varchar2(200),
  Opini�n_usuario varchar2(200),
  
  constraint res_pk primary key(Id_restaurante)
);

 


create table Tener_restaurante
(
  Id_restaurante int,
  Nombre_ciudad varchar2(200),
  
  constraint res_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint ciudadRes_fk2 foreign key (Nombre_ciudad) references  Ciudades_Pueblos (Nombre)
);

  alter table Tener_restaurante add constraint entre_ciudad_pk primary key (Id_restaurante,Nombre_ciudad);
  
create table Bares
(
  Id_bar int,
  Nombre varchar2(100),
  Contacto int,
  Especialidad varchar2(100), 
  Precio int,
  Horario varchar2(50),
  Descripcion varchar2(200),
  opini�n_usuario varchar2(300),
 
  constraint barPais_pk primary key (Id_bar)
);

create table Tener_bar
(
  Id_bar int,
  Nombre_ciudad varchar2(200),
  
  constraint bares_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint ciudadBar_fk2 foreign key (Nombre_ciudad) references  Ciudades_Pueblos (Nombre)
);
  alter table Tener_bar add constraint bar_ciudad_pk primary key (Id_bar,Nombre_ciudad);


Create table Entretenimiento
(
  Id_entre int,
  Tipo varchar2(200),
  Descripcion varchar2(200), 
  Horario varchar2(200),
  Opinion varchar2(200),

  constraint entret_pk primary key(Id_entre)
);

create table Tener_entretenimiento
(
  Id_entre int,
  Nombre_ciudad varchar2(200),
  constraint enter_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint ciudadEnt_fk2 foreign key (Nombre_ciudad) references  Ciudades_Pueblos(Nombre)
);

  alter table Tener_entretenimiento add constraint entre_2ciudad_pk primary key (Id_entre,Nombre_ciudad);
  

create table Servicios 
(
  Id_servicio int,
  Tipo varchar2(200),
  Contacto int,
  
  constraint servicio_pk primary key(Id_servicio)
);

create table Tener_servicios
(
  id_servicio int,
  Nombre_ciudad varchar2(200),
  
  constraint serv_fk1 foreign key (id_servicio) references Servicios (Id_servicio),
  constraint ciudadServicio_fk2 foreign key (Nombre_ciudad) references  Ciudades_Pueblos (Nombre)
);
  alter table Tener_servicios add constraint serv_ciudad_pk primary key (Id_servicio,Nombre_ciudad);

/*Tuplas por defecto*/ 
  Dni varchar2(9) not null,
  clave varchar2(10)  not null UNIQUE,
  usuario varchar2(10)  not null UNIQUE,
  Nombre varchar2(50)  constraint nombre_Nn not null,
  Apellido varchar2(50)  constraint Apellido_Nn not null,
  foto blob,
  dni_superusuario varchar2(9),
  
  constraint DniAdmin_pk primary key (Dni)

insert into Administradores values ('386743G','hol','DOS','JOSE','HDASG',null,'386743G');
insert into Administradores values ('33453G','adew','cinco','Miguel','gfd43',null,'386743G');

insert into Usuarios values ('54355t','Emma','ghasjk','jhfg@hotmal.com',43006,'kfhdsk','ldldls','hola todos','386743G');
insert into Usuarios values ('43567d','Romi','mont','j56fg@hotmal.com',43006,'563fdg','lgfdg5','hola todos','33453G');


insert into Paises values ('Espanya','Euro','Espa�ol',null,'Madrid',346278,'Europa');
insert into Paises values ('Alemanya','Euro','Alemany',null,null,346278,'Europa');


insert into Ciudades_Pueblos values ('Tarragona','Espanya','Madrid');
insert into Ciudades_Pueblos values ('Valencia','Espanya','Madrid');

insert into Hoteles values (1,'hola',3748833,'Todo',null,'hola a todos','hola askdjasoi');
insert into Hoteles values (2,'Adew',536534,'Todo',null,'hola a todos','hola askdjasoi');
  
insert into Tener_hotel values (1,'Tarragona');
insert into Tener_hotel values (2,'Valencia');

insert into Restaurantes values (1,'Tantra',985263514,'Hind�','12-20�','13-02', 'hdgsfhjkdshfk','hjgbhjhj');
insert into Restaurantes values (2,'Aqua',925893612,'Americana','8-15�','20-02', 'hdhkj','hjdytyuhj');

insert into Tener_restaurante values (1,'Valencia');
insert into Tener_restaurante values (2,'Tarragona');

insert into Bares values (1,'Comete algo',933645270,'Platos combinados',10,'15-00','hjjkhjkhk', 'ghjgjghj');
insert into Bares values (2,'Quijote',985126390,'Tapas',12,'11-02','hioipk', 'gl�lk�kljklhj');

insert into Tener_bar values (1,'Valencia');
insert into Tener_bar values (2,'Tarragona');

insert into Entretenimiento values (1,'Entretenimiento','dfghjk','18','ertyui  yurfytdsyu g');
insert into Entretenimiento values (2,'Entretenimiento2','dfghj','1','ertyui  yurfytdsyu sf');

insert into Tener_entretenimiento values (1,'Valencia');
insert into Tener_entretenimiento values (2,'Tarragona');

insert into Servicios values (1,'yoquese',3);
insert into Servicios values (3,'yoquese1',2);

insert into tener_servicios values (1,'Valencia');
insert into tener_servicios values (3,'Tarragona');

