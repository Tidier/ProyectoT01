create table Administradores
(
  clave varchar2(10),
  usuario varchar2(10),
  Dni varchar2(9),
  Nombre varchar2(50),
  Apellido varchar2(50),
  foto blob,
  dni_superusuario varchar2(9),
  
  constraint clave_pk primary key (Dni)
);

  alter table Administradores add foreign key (dni_superusuario) references Administradores (Dni);


create table Usuarios
(
  Usuario varchar2(20),
  Clave varchar(10),
  Nombre varchar2(50),
  Apellido varchar2(50),
  Email varchar(100),
  Codigo_postal int,
  Comentarios varchar2(250),
  Dni_admin varchar2(9),
  Ultimo_viaje varchar2(200),
  Pais_destino varchar2 (300),
  Otros_datos varchar2(300),
  Foto blob,
  constraint usuari_pk primary key (Usuario,Clave),
  constraint adm_fk foreign key (Dni_admin) references Administradores(Dni)
);



create table Paises
(
  Nombre varchar2(200),
  Moneda varchar2(200),
  Idioma varchar2(200),
  Bandera varchar2(200),
  Capital varchar2(200),
  Habitantes int,
  Nombre_cont varchar2(200),
  id_ciudad int,
  
  constraint nom_pk primary key (Nombre),
  constraint ciudad_id_fk foreign key (id_ciudad) references Paises(id_ciudad)

);

create table Ciudades_Pueblos
(
  id_ciudad int,
  Nombre varchar2(200),
  descripcion varchar2(300),
  Nombre_pais varchar2(200),
    
    constraint nomPa_pk primary key (id_ciudad),
    constraint paisNom_fk foreign key (Nombre_pais) references Paises(Nombre)
);

Create table Hoteles
(
    Id_hotel int,
    Nombre varchar2(250),
    Contacto int, 
    Servicios varchar2(200), 
    Tarifas varchar2(100), 
    Descripcion varchar2(200),
    Foto blob,
    constraint hotel_pk primary key(Id_hotel)
);

Create table Tener_hotel
(
  Id_hotel int,
  id_ciudad int,
  
  constraint hot_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
  constraint ciudad_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_hotel add constraint hot_ciudad_pk primary key (Id_hotel,Nombre_ciudad);
  
create table Opinar_hotel
(
  Id_hotel int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint hotel_user_fk1 foreign key (Id_hotel) references Hoteles (Id_hotel),
  constraint opinarUsuario_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_hotel add constraint op_hot_user_pk primary key (Id_hotel,Clave,Usuario);

Create table Restaurantes
(
  Id_restaurante int,
  Nombre varchar2(200),
  Contacto int,
  Especialidad varchar2(200),
  Tarifas varchar2(200), 
  Horario varchar2(200), 
  Descripcion varchar2(200),
  foto blob,
  
  constraint res_pk primary key(Id_restaurante)
);

 


create table Tener_restaurante
(
  Id_restaurante int,
  id_ciudad int,
  
  constraint res_user_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint ciudad_resta_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_restaurante add constraint entre_ciudad_pk primary key (Id_restaurante,id_ciudad);
  
  
  create table Opinar_restuarante
(
  Id_restaurante int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint res_user_op_fk1 foreign key (Id_restaurante) references Restaurantes (Id_restaurante),
  constraint opinar_Usuario_rest_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_restuarante add constraint op_resta_user_pk primary key ( Id_restaurante,Clave,Usuario);
  
create table Bares
(
  Id_bar int,
  Nombre varchar2(100),
  Contacto int,
  Especialidad varchar2(100), 
  Precio int,
  Horario varchar2(50),
  Descripcion varchar2(200),
  foto blob,
 
  constraint barPais_pk primary key (Id_bar)
);

create table Tener_bar
(
  Id_bar int,
  id_ciudad int,
  
  constraint bares_user_op_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint ciudad_bar_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_bar add constraint bar_ciudad_pk primary key (Id_bar,id_ciudad);
  
  create table Opinar_bar
(
  Id_bar int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint bar_fk1 foreign key (Id_bar) references Bares (Id_bar),
  constraint opinarUsuario_bar_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_bar add constraint op_bar_user_pk primary key ( Id_bar,Clave,Usuario);
  


Create table Entretenimiento
(
  Id_entre int,
  Tipo varchar2(200),
  Descripcion varchar2(200), 
  Horario varchar2(200),
  Contacto int,
  foto blob,

  constraint entret_pk primary key(Id_entre)
);

create table Tener_entretenimiento
(
  Id_entre int,
  id_ciudad int,
  constraint enter_user_op_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint ciudad_entre_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);

  alter table Tener_entretenimiento add constraint entre_2ciudad_pk primary key (Id_entre,id_ciudad);
  
  
 create table Opinar_entretenimiento
(
  Id_entre int,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint enter_fk1 foreign key (id_entre) references Entretenimiento (Id_entre),
  constraint opinarUsuario_entre_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_entretenimiento add constraint op_entre_user_pk primary key ( Id_entre,Clave,Usuario);
  

create table Servicios 
(
  Id_servicio int,
  Tipo varchar2(200),
  descripcion varchar2(300),
  Contacto int,
  horario varchar2(100),
  foto blob,
  
  constraint servicio_pk primary key(Id_servicio)
);

create table Tener_servicios
(
  id_servicio int,
  id_ciudad int,
  
  constraint serv_user_fk1 foreign key (id_servicio) references Servicios (Id_servicio),
  constraint ciudad_ser_op_fk2 foreign key (id_ciudad) references  Ciudades_Pueblos(id_ciudad)
);
  alter table Tener_servicios add constraint serv_ciudad_pk primary key (Id_servicio,id_ciudad);


 create table Opinar_servicios
(
  Id_servicio,
  Usuario varchar2(20),
  Clave varchar2(10),
  Opinion varchar2(300),
  data_comentario date,
  
  constraint serv_user_op_fk1 foreign key (Id_servicio) references Servicios (Id_servicio),
  constraint opinarUsuario_serv_fk2 foreign key (Clave,Usuario) references  Usuarios (Clave,Usuario)
);
  alter table Opinar_servicios add constraint op_serv_user_pk primary key ( Id_servicio,Clave,Usuario);
  

/*Tuplas por defecto*/ 

insert into Administradores values ('hol','DOS','386743G','JOSE','HDASG',null,'386743G');
insert into Administradores values ('adew','cinco','33453G','Miguel','gfd43',null,'33453G');

insert into Usuarios values ('54355t','Emma','ghasjk','jhfg@hotmal.com',43006,'kfhdsk','ldldls','hola todos','386743G');
insert into Usuarios values ('43567d','Romi','mont','j56fg@hotmal.com',43006,'563fdg','lgfdg5','hola todos','33453G');


insert into Paises values ('Espanya','Euro','Espa�ol',null,'Madrid',346278,'Europa');
insert into Paises values ('Alemanya','Euro','Alemany',null,null,346278,'Europa');


insert into Ciudades_Pueblos values ('Tarragona','Espanya','Madrid');
insert into Ciudades_Pueblos values ('Valencia','Espanya','Madrid');

insert into Hoteles values (1,'hola',3748833,'Todo',null,'hola a todos','hola askdjasoi');
insert into Hoteles values (2,'Adew',536534,'Todo',null,'hola a todos','hola askdjasoi');
  
insert into Tener_hotel values (1,'Tarragona');
insert into Tener_hotel values (2,'Valencia');

insert into Restaurantes values (1,'Tantra',985263514,'Hind�','12-20�','13-02', 'hdgsfhjkdshfk','hjgbhjhj');
insert into Restaurantes values (2,'Aqua',925893612,'Americana','8-15�','20-02', 'hdhkj','hjdytyuhj');

insert into Tener_restaurante values (1,'Valencia');
insert into Tener_restaurante values (2,'Tarragona');

insert into Bares values (1,'Comete algo',933645270,'Platos combinados',10,'15-00','hjjkhjkhk', 'ghjgjghj');
insert into Bares values (2,'Quijote',985126390,'Tapas',12,'11-02','hioipk', 'gl�lk�kljklhj');

insert into Tener_bar values (1,'Valencia');
insert into Tener_bar values (2,'Tarragona');

insert into Entretenimiento values (1,'Entretenimiento','dfghjk','18','ertyui  yurfytdsyu g');
insert into Entretenimiento values (2,'Entretenimiento2','dfghj','1','ertyui  yurfytdsyu sf');

insert into Tener_entretenimiento values (1,'Valencia');
insert into Tener_entretenimiento values (2,'Tarragona');

insert into Servicios values (1,'yoquese',3);
insert into Servicios values (3,'yoquese1',2);

insert into tener_servicios values (1,'Valencia');
insert into tener_servicios values (3,'Tarragona');
